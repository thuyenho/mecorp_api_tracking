(function() {
    var sAsync = {},
        root = this,
        presiousSAsync;

    function _endFunction(end) {
        function _emptyFunction() {}

        return (typeof end === 'function') ? end : _emptyFunction;
    }

    function _checkMethods(methods) {
        var methodsLength = methods && methods.length,
            correct = !!methodsLength,
            i = 0;

        while (correct && (i < methodsLength)) {
            correct &= (typeof methods[i] === 'function');
            i++;
        }
        return correct;
    }

    if (root != null) {
        presiousSAsync = root.sAsync;
    }

    /**
     * Returns the simple-async object without overwriting other one.
     *
     * @return {object} Simple-async object.
     */
    sAsync.noConflict = function() {
        root.sAsync = presiousSAsync;
        return sAsync;
    };

    /**
     * Executes asynchronously the array of methods in series and finally call the
     * end method with the result parameter. If there is any error, the 'end'
     * method will be executed before finishing the remaining unfinished methods.
     *
     * @param {function[]} methods Array of methods, example: 'function(next) {}',
     *   the 'next' parameter should be invoqued after method is completed to
     *   execute the next method, if it has a parameter there is an error.
     * @param {function} end Ending method: 'function(result) {}', the 'result'
     *   parameter can be undefined or other value according to a success or
     *   unsuccess result respectively.
     */
    sAsync.doSeries = function(methods, end) {
        var _end = _endFunction(end),
            methodsLength = methods && methods.length,
            methodIndex = 0;

        function _execute(methodIndex) {
            if (methodIndex < methodsLength) {
                methods[methodIndex++](_next);
            } else {
                _end();
            }
        }

        function _next(error) {
            if (typeof error === 'undefined') {
                _execute(methodIndex++);
            } else {
                _end(error);
            }
        }

        if (_checkMethods(methods)) {
            _next();
        } else {
            _next(false);
        }
    };

    /**
     * Executes asynchronously the array of methods in parallel and finally call
     * the end method with the result parameter. If there is any error, the 'end'
     * method will be executed before finishing the remaining unfinished methods.
     *
     * @param {function[]} methods Array of methods, example: 'function(done) {}',
     *   the 'done' parameter should be invoqued after method is completed to
     *   execute the callback, if it has a parameter there is an error.
     * @param {function} end Ending method: 'function(result) {}', the 'result'
     *   parameter can be undefined or other value according to a success or
     *   unsuccess result respectively.
     */
    sAsync.doParallel = function(methods, end) {
        var _end = _endFunction(end),
            endCalled = false,
            methodsLength = methods && methods.length,
            finishedMethods = 0,
            i = 0;

        function _done(error) {
            var _error = (typeof error !== 'undefined'),
                _finished = (++finishedMethods === methodsLength);

            if (!endCalled && (_finished || _error)) {
                endCalled = true;
                if (_error) {
                    _end(error);
                } else {
                    _end();
                }
            }
        }

        if (_checkMethods(methods)) {
            i = 0;
            while (i < methodsLength) {
                methods[i++](_done);
            }
        } else {
            _end(false);
        }
    };

    // Make sAsync library visible.
    if ((typeof define !== 'undefined') && define.amd) { // AMD / RequireJS.
        define([], function() {
            return sAsync;
        });
    } else if ((typeof module !== 'undefined') && module.exports) { // NodeJS.
        module.exports = sAsync;
    } else { // Browser.
        root.sAsync = sAsync;
    }
}());

/*
 * Fingerprintjs2 0.7.1 - Modern & flexible browser fingerprint library v2
 * https://github.com/Valve/fingerprintjs2
 * Copyright (c) 2015 Valentin Vasilyev (valentin.vasilyev@outlook.com)
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) license.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL VALENTIN VASILYEV BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

(function (name, context, definition) {
    "use strict";
    if (typeof module !== "undefined" && module.exports) { module.exports = definition(); }
    else if (typeof define === "function" && define.amd) { define(definition); }
    else { context[name] = definition(); }
})("Fingerprint2", this, function() {
    "use strict";
    // This will only be polyfilled for IE8 and older
    // Taken from Mozilla MDC
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function(searchElement, fromIndex) {
            var k;
            if (this == null) {
                throw new TypeError("'this' is null or undefined");
            }
            var O = Object(this);
            var len = O.length >>> 0;
            if (len === 0) {
                return -1;
            }
            var n = +fromIndex || 0;
            if (Math.abs(n) === Infinity) {
                n = 0;
            }
            if (n >= len) {
                return -1;
            }
            k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
            while (k < len) {
                if (k in O && O[k] === searchElement) {
                    return k;
                }
                k++;
            }
            return -1;
        };
    }
    var Fingerprint2 = function(options) {
        var defaultOptions = {
            swfContainerId: "fingerprintjs2",
            swfPath: "flash/compiled/FontList.swf",
            sortPluginsFor: [/palemoon/i]
        };
        this.options = this.extend(options, defaultOptions);
        this.nativeForEach = Array.prototype.forEach;
        this.nativeMap = Array.prototype.map;
    };
    Fingerprint2.prototype = {
        extend: function(source, target) {
            if (source == null) { return target; }
            for (var k in source) {
                if(source[k] != null && target[k] !== source[k]) {
                    target[k] = source[k];
                }
            }
            return target;
        },
        log: function(msg){
            if(window.console){
                console.log(msg);
            }
        },
        get: function(done){
            var keys = [];
            keys = this.userAgentKey(keys);
            keys = this.languageKey(keys);
            keys = this.colorDepthKey(keys);
            keys = this.screenResolutionKey(keys);
            keys = this.timezoneOffsetKey(keys);
            keys = this.sessionStorageKey(keys);
            keys = this.localStorageKey(keys);
            keys = this.indexedDbKey(keys);
            keys = this.addBehaviorKey(keys);
            keys = this.openDatabaseKey(keys);
            keys = this.cpuClassKey(keys);
            keys = this.platformKey(keys);
            keys = this.doNotTrackKey(keys);
            keys = this.pluginsKey(keys);
            keys = this.canvasKey(keys);
            keys = this.webglKey(keys);
            keys = this.adBlockKey(keys);
            keys = this.hasLiedLanguagesKey(keys);
            keys = this.hasLiedResolutionKey(keys);
            keys = this.hasLiedOsKey(keys);
            keys = this.hasLiedBrowserKey(keys);
            keys = this.touchSupportKey(keys);
            var that = this;
            this.fontsKey(keys, function(newKeys){
                var murmur = that.x64hash128(newKeys.join("~~~"), 31);
                return done(murmur);
            });
        },
        userAgentKey: function(keys) {
            if(!this.options.excludeUserAgent) {
                keys.push(this.getUserAgent());
            }
            return keys;
        },
        // for tests
        getUserAgent: function(){
            return navigator.userAgent;
        },
        languageKey: function(keys) {
            if(!this.options.excludeLanguage) {
                keys.push(navigator.language);
            }
            return keys;
        },
        colorDepthKey: function(keys) {
            if(!this.options.excludeColorDepth) {
                keys.push(screen.colorDepth);
            }
            return keys;
        },
        screenResolutionKey: function(keys) {
            if(!this.options.excludeScreenResolution) {
                return this.getScreenResolution(keys);
            }
            return keys;
        },
        getScreenResolution: function(keys) {
            var resolution;
            var available;
            if(this.options.detectScreenOrientation) {
                resolution = (screen.height > screen.width) ? [screen.height, screen.width] : [screen.width, screen.height];
            } else {
                resolution = [screen.height, screen.width];
            }
            if(typeof resolution !== "undefined") { // headless browsers
                keys.push(resolution);
            }
            if(screen.availWidth && screen.availHeight) {
                if(this.options.detectScreenOrientation) {
                    available = (screen.availHeight > screen.availWidth) ? [screen.availHeight, screen.availWidth] : [screen.availWidth, screen.availHeight];
                } else {
                    available = [screen.availHeight, screen.availWidth];
                }
            }
            if(typeof available !== "undefined") { // headless browsers
                keys.push(available);
            }
            return keys;
        },
        timezoneOffsetKey: function(keys) {
            if(!this.options.excludeTimezoneOffset) {
                keys.push(new Date().getTimezoneOffset());
            }
            return keys;
        },
        sessionStorageKey: function(keys) {
            if(!this.options.excludeSessionStorage && this.hasSessionStorage()) {
                keys.push("sessionStorageKey");
            }
            return keys;
        },
        localStorageKey: function(keys) {
            if(!this.options.excludeSessionStorage && this.hasLocalStorage()) {
                keys.push("localStorageKey");
            }
            return keys;
        },
        indexedDbKey: function(keys) {
            if(!this.options.excludeIndexedDB && this.hasIndexedDB()) {
                keys.push("indexedDbKey");
            }
            return keys;
        },
        addBehaviorKey: function(keys) {
            //body might not be defined at this point or removed programmatically
            if(document.body && !this.options.excludeAddBehavior && document.body.addBehavior) {
                keys.push("addBehaviorKey");
            }
            return keys;
        },
        openDatabaseKey: function(keys) {
            if(!this.options.excludeOpenDatabase && window.openDatabase) {
                keys.push("openDatabase");
            }
            return keys;
        },
        cpuClassKey: function(keys) {
            if(!this.options.excludeCpuClass) {
                keys.push(this.getNavigatorCpuClass());
            }
            return keys;
        },
        platformKey: function(keys) {
            if(!this.options.excludePlatform) {
                keys.push(this.getNavigatorPlatform());
            }
            return keys;
        },
        doNotTrackKey: function(keys) {
            if(!this.options.excludeDoNotTrack) {
                keys.push(this.getDoNotTrack());
            }
            return keys;
        },
        canvasKey: function(keys) {
            if(!this.options.excludeCanvas && this.isCanvasSupported()) {
                keys.push(this.getCanvasFp());
            }
            return keys;
        },
        webglKey: function(keys) {
            if(this.options.excludeWebGL) {
                if(typeof NODEBUG === "undefined"){
                    this.log("Skipping WebGL fingerprinting per excludeWebGL configuration option");
                }
                return keys;
            }
            if(!this.isWebGlSupported()) {
                if(typeof NODEBUG === "undefined"){
                    this.log("Skipping WebGL fingerprinting because it is not supported in this browser");
                }
                return keys;
            }
            keys.push(this.getWebglFp());
            return keys;
        },
        adBlockKey: function(keys){
            if(!this.options.excludeAdBlock) {
                keys.push(this.getAdBlock());
            }
            return keys;
        },
        hasLiedLanguagesKey: function(keys){
            if(!this.options.excludeHasLiedLanguages){
                keys.push(this.getHasLiedLanguages());
            }
            return keys;
        },
        hasLiedResolutionKey: function(keys){
            if(!this.options.excludeHasLiedResolution){
                keys.push(this.getHasLiedResolution());
            }
            return keys;
        },
        hasLiedOsKey: function(keys){
            if(!this.options.excludeHasLiedOs){
                keys.push(this.getHasLiedOs());
            }
            return keys;
        },
        hasLiedBrowserKey: function(keys){
            if(!this.options.excludeHasLiedBrowser){
                keys.push(this.getHasLiedBrowser());
            }
            return keys;
        },
        fontsKey: function(keys, done) {
            if (this.options.excludeJsFonts) {
                return this.flashFontsKey(keys, done);
            }
            return this.jsFontsKey(keys, done);
        },
        // flash fonts (will increase fingerprinting time 20X to ~ 130-150ms)
        flashFontsKey: function(keys, done) {
            if(this.options.excludeFlashFonts) {
                if(typeof NODEBUG === "undefined"){
                    this.log("Skipping flash fonts detection per excludeFlashFonts configuration option");
                }
                return done(keys);
            }
            // we do flash if swfobject is loaded
            if(!this.hasSwfObjectLoaded()){
                if(typeof NODEBUG === "undefined"){
                    this.log("Swfobject is not detected, Flash fonts enumeration is skipped");
                }
                return done(keys);
            }
            if(!this.hasMinFlashInstalled()){
                if(typeof NODEBUG === "undefined"){
                    this.log("Flash is not installed, skipping Flash fonts enumeration");
                }
                return done(keys);
            }
            if(typeof this.options.swfPath === "undefined"){
                if(typeof NODEBUG === "undefined"){
                    this.log("To use Flash fonts detection, you must pass a valid swfPath option, skipping Flash fonts enumeration");
                }
                return done(keys);
            }
            this.loadSwfAndDetectFonts(function(fonts){
                keys.push(fonts.join(";"));
                done(keys);
            });
        },
        // kudos to http://www.lalit.org/lab/javascript-css-font-detect/
        jsFontsKey: function(keys, done) {
            // doing js fonts detection in a pseudo-async fashion
            return setTimeout(function(){

                // a font will be compared against all the three default fonts.
                // and if it doesn't match all 3 then that font is not available.
                var baseFonts = ["monospace", "sans-serif", "serif"];

                //we use m or w because these two characters take up the maximum width.
                // And we use a LLi so that the same matching fonts can get separated
                var testString = "mmmmmmmmmmlli";

                //we test using 72px font size, we may use any size. I guess larger the better.
                var testSize = "72px";

                var h = document.getElementsByTagName("body")[0];

                // create a SPAN in the document to get the width of the text we use to test
                var s = document.createElement("span");
                s.style.fontSize = testSize;
                s.innerHTML = testString;
                var defaultWidth = {};
                var defaultHeight = {};
                for (var index in baseFonts) {
                    //get the default width for the three base fonts
                    s.style.fontFamily = baseFonts[index];
                    h.appendChild(s);
                    defaultWidth[baseFonts[index]] = s.offsetWidth; //width for the default font
                    defaultHeight[baseFonts[index]] = s.offsetHeight; //height for the defualt font
                    h.removeChild(s);
                }
                var detect = function (font) {
                    var detected = false;
                    for (var index in baseFonts) {
                        s.style.fontFamily = font + "," + baseFonts[index]; // name of the font along with the base font for fallback.
                        h.appendChild(s);
                        var matched = (s.offsetWidth !== defaultWidth[baseFonts[index]] || s.offsetHeight !== defaultHeight[baseFonts[index]]);
                        h.removeChild(s);
                        detected = detected || matched;
                    }
                    return detected;
                };
                var fontList = [
                    "Abadi MT Condensed Light", "Academy Engraved LET", "ADOBE CASLON PRO", "Adobe Garamond", "ADOBE GARAMOND PRO", "Agency FB", "Aharoni", "Albertus Extra Bold", "Albertus Medium", "Algerian", "Amazone BT", "American Typewriter",
                    "American Typewriter Condensed", "AmerType Md BT", "Andale Mono", "Andalus", "Angsana New", "AngsanaUPC", "Antique Olive", "Aparajita", "Apple Chancery", "Apple Color Emoji", "Apple SD Gothic Neo", "Arabic Typesetting", "ARCHER", "Arial", "Arial Black", "Arial Hebrew",
                    "Arial MT", "Arial Narrow", "Arial Rounded MT Bold", "Arial Unicode MS", "ARNO PRO", "Arrus BT", "Aurora Cn BT", "AvantGarde Bk BT", "AvantGarde Md BT", "AVENIR", "Ayuthaya", "Bandy", "Bangla Sangam MN", "Bank Gothic", "BankGothic Md BT", "Baskerville",
                    "Baskerville Old Face", "Batang", "BatangChe", "Bauer Bodoni", "Bauhaus 93", "Bazooka", "Bell MT", "Bembo", "Benguiat Bk BT", "Berlin Sans FB", "Berlin Sans FB Demi", "Bernard MT Condensed", "BernhardFashion BT", "BernhardMod BT", "Big Caslon", "BinnerD",
                    "Bitstream Vera Sans Mono", "Blackadder ITC", "BlairMdITC TT", "Bodoni 72", "Bodoni 72 Oldstyle", "Bodoni 72 Smallcaps", "Bodoni MT", "Bodoni MT Black", "Bodoni MT Condensed", "Bodoni MT Poster Compressed", "Book Antiqua", "Bookman Old Style",
                    "Bookshelf Symbol 7", "Boulder", "Bradley Hand", "Bradley Hand ITC", "Bremen Bd BT", "Britannic Bold", "Broadway", "Browallia New", "BrowalliaUPC", "Brush Script MT", "Calibri", "Californian FB", "Calisto MT", "Calligrapher", "Cambria", "Cambria Math", "Candara",
                    "CaslonOpnface BT", "Castellar", "Centaur", "Century", "Century Gothic", "Century Schoolbook", "Cezanne", "CG Omega", "CG Times", "Chalkboard", "Chalkboard SE", "Chalkduster", "Charlesworth", "Charter Bd BT", "Charter BT", "Chaucer",
                    "ChelthmITC Bk BT", "Chiller", "Clarendon", "Clarendon Condensed", "CloisterBlack BT", "Cochin", "Colonna MT", "Comic Sans", "Comic Sans MS", "Consolas", "Constantia", "Cooper Black", "Copperplate", "Copperplate Gothic", "Copperplate Gothic Bold",
                    "Copperplate Gothic Light", "CopperplGoth Bd BT", "Corbel", "Cordia New", "CordiaUPC", "Cornerstone", "Coronet", "Courier", "Courier New", "Cuckoo", "Curlz MT", "DaunPenh", "Dauphin", "David", "DB LCD Temp", "DELICIOUS", "Denmark", "Devanagari Sangam MN",
                    "DFKai-SB", "Didot", "DilleniaUPC", "DIN", "DokChampa", "Dotum", "DotumChe", "Ebrima", "Edwardian Script ITC", "Elephant", "English 111 Vivace BT", "Engravers MT", "EngraversGothic BT", "Eras Bold ITC", "Eras Demi ITC", "Eras Light ITC", "Eras Medium ITC",
                    "Estrangelo Edessa", "EucrosiaUPC", "Euphemia", "Euphemia UCAS", "EUROSTILE", "Exotc350 Bd BT", "FangSong", "Felix Titling", "Fixedsys", "FONTIN", "Footlight MT Light", "Forte", "Franklin Gothic", "Franklin Gothic Book", "Franklin Gothic Demi",
                    "Franklin Gothic Demi Cond", "Franklin Gothic Heavy", "Franklin Gothic Medium", "Franklin Gothic Medium Cond", "FrankRuehl", "Fransiscan", "Freefrm721 Blk BT", "FreesiaUPC", "Freestyle Script", "French Script MT", "FrnkGothITC Bk BT", "Fruitger", "FRUTIGER",
                    "Futura", "Futura Bk BT", "Futura Lt BT", "Futura Md BT", "Futura ZBlk BT", "FuturaBlack BT", "Gabriola", "Galliard BT", "Garamond", "Gautami", "Geeza Pro", "Geneva", "Geometr231 BT", "Geometr231 Hv BT", "Geometr231 Lt BT", "Georgia", "GeoSlab 703 Lt BT",
                    "GeoSlab 703 XBd BT", "Gigi", "Gill Sans", "Gill Sans MT", "Gill Sans MT Condensed", "Gill Sans MT Ext Condensed Bold", "Gill Sans Ultra Bold", "Gill Sans Ultra Bold Condensed", "Gisha", "Gloucester MT Extra Condensed", "GOTHAM", "GOTHAM BOLD",
                    "Goudy Old Style", "Goudy Stout", "GoudyHandtooled BT", "GoudyOLSt BT", "Gujarati Sangam MN", "Gulim", "GulimChe", "Gungsuh", "GungsuhChe", "Gurmukhi MN", "Haettenschweiler", "Harlow Solid Italic", "Harrington", "Heather", "Heiti SC", "Heiti TC", "HELV", "Helvetica",
                    "Helvetica Neue", "Herald", "High Tower Text", "Hiragino Kaku Gothic ProN", "Hiragino Mincho ProN", "Hoefler Text", "Humanst 521 Cn BT", "Humanst521 BT", "Humanst521 Lt BT", "Impact", "Imprint MT Shadow", "Incised901 Bd BT", "Incised901 BT",
                    "Incised901 Lt BT", "INCONSOLATA", "Informal Roman", "Informal011 BT", "INTERSTATE", "IrisUPC", "Iskoola Pota", "JasmineUPC", "Jazz LET", "Jenson", "Jester", "Jokerman", "Juice ITC", "Kabel Bk BT", "Kabel Ult BT", "Kailasa", "KaiTi", "Kalinga", "Kannada Sangam MN",
                    "Kartika", "Kaufmann Bd BT", "Kaufmann BT", "Khmer UI", "KodchiangUPC", "Kokila", "Korinna BT", "Kristen ITC", "Krungthep", "Kunstler Script", "Lao UI", "Latha", "Leelawadee", "Letter Gothic", "Levenim MT", "LilyUPC", "Lithograph", "Lithograph Light", "Long Island",
                    "Lucida Bright", "Lucida Calligraphy", "Lucida Console", "Lucida Fax", "LUCIDA GRANDE", "Lucida Handwriting", "Lucida Sans", "Lucida Sans Typewriter", "Lucida Sans Unicode", "Lydian BT", "Magneto", "Maiandra GD", "Malayalam Sangam MN", "Malgun Gothic",
                    "Mangal", "Marigold", "Marion", "Marker Felt", "Market", "Marlett", "Matisse ITC", "Matura MT Script Capitals", "Meiryo", "Meiryo UI", "Microsoft Himalaya", "Microsoft JhengHei", "Microsoft New Tai Lue", "Microsoft PhagsPa", "Microsoft Sans Serif", "Microsoft Tai Le",
                    "Microsoft Uighur", "Microsoft YaHei", "Microsoft Yi Baiti", "MingLiU", "MingLiU_HKSCS", "MingLiU_HKSCS-ExtB", "MingLiU-ExtB", "Minion", "Minion Pro", "Miriam", "Miriam Fixed", "Mistral", "Modern", "Modern No. 20", "Mona Lisa Solid ITC TT", "Monaco", "Mongolian Baiti",
                    "MONO", "Monotype Corsiva", "MoolBoran", "Mrs Eaves", "MS Gothic", "MS LineDraw", "MS Mincho", "MS Outlook", "MS PGothic", "MS PMincho", "MS Reference Sans Serif", "MS Reference Specialty", "MS Sans Serif", "MS Serif", "MS UI Gothic", "MT Extra", "MUSEO", "MV Boli", "MYRIAD",
                    "MYRIAD PRO", "Nadeem", "Narkisim", "NEVIS", "News Gothic", "News GothicMT", "NewsGoth BT", "Niagara Engraved", "Niagara Solid", "Noteworthy", "NSimSun", "Nyala", "OCR A Extended", "Old Century", "Old English Text MT", "Onyx", "Onyx BT", "OPTIMA", "Oriya Sangam MN",
                    "OSAKA", "OzHandicraft BT", "Palace Script MT", "Palatino", "Palatino Linotype", "Papyrus", "Parchment", "Party LET", "Pegasus", "Perpetua", "Perpetua Titling MT", "PetitaBold", "Pickwick", "Plantagenet Cherokee", "Playbill", "PMingLiU", "PMingLiU-ExtB",
                    "Poor Richard", "Poster", "PosterBodoni BT", "PRINCETOWN LET", "Pristina", "PTBarnum BT", "Pythagoras", "Raavi", "Rage Italic", "Ravie", "Ribbon131 Bd BT", "Rockwell", "Rockwell Condensed", "Rockwell Extra Bold", "Rod", "Roman", "Sakkal Majalla",
                    "Santa Fe LET", "Savoye LET", "Sceptre", "Script", "Script MT Bold", "SCRIPTINA", "Segoe Print", "Segoe Script", "Segoe UI", "Segoe UI Light", "Segoe UI Semibold", "Segoe UI Symbol", "Serifa", "Serifa BT", "Serifa Th BT", "ShelleyVolante BT", "Sherwood",
                    "Shonar Bangla", "Showcard Gothic", "Shruti", "Signboard", "SILKSCREEN", "SimHei", "Simplified Arabic", "Simplified Arabic Fixed", "SimSun", "SimSun-ExtB", "Sinhala Sangam MN", "Sketch Rockwell", "Skia", "Small Fonts", "Snap ITC", "Snell Roundhand", "Socket",
                    "Souvenir Lt BT", "Staccato222 BT", "Steamer", "Stencil", "Storybook", "Styllo", "Subway", "Swis721 BlkEx BT", "Swiss911 XCm BT", "Sylfaen", "Synchro LET", "System", "Tahoma", "Tamil Sangam MN", "Technical", "Teletype", "Telugu Sangam MN", "Tempus Sans ITC",
                    "Terminal", "Thonburi", "Times", "Times New Roman", "Times New Roman PS", "Traditional Arabic", "Trajan", "TRAJAN PRO", "Trebuchet MS", "Tristan", "Tubular", "Tunga", "Tw Cen MT", "Tw Cen MT Condensed", "Tw Cen MT Condensed Extra Bold",
                    "TypoUpright BT", "Unicorn", "Univers", "Univers CE 55 Medium", "Univers Condensed", "Utsaah", "Vagabond", "Vani", "Verdana", "Vijaya", "Viner Hand ITC", "VisualUI", "Vivaldi", "Vladimir Script", "Vrinda", "Westminster", "WHITNEY", "Wide Latin", "Wingdings",
                    "Wingdings 2", "Wingdings 3", "ZapfEllipt BT", "ZapfHumnst BT", "ZapfHumnst Dm BT", "Zapfino", "Zurich BlkEx BT", "Zurich Ex BT", "ZWAdobeF"];
                var available = [];
                for (var i = 0, l = fontList.length; i < l; i++) {
                    if(detect(fontList[i])) {
                        available.push(fontList[i]);
                    }
                }
                keys.push(available.join(";"));
                done(keys);
            }, 1);
        },
        pluginsKey: function(keys) {
            if(this.isIE()){
                keys.push(this.getIEPluginsString());
            } else {
                keys.push(this.getRegularPluginsString());
            }
            return keys;
        },
        getRegularPluginsString: function () {
            var plugins = [];
            for(var i = 0, l = navigator.plugins.length; i < l; i++) {
                plugins.push(navigator.plugins[i]);
            }
            // sorting plugins only for those user agents, that we know randomize the plugins
            // every time we try to enumerate them
            if(this.pluginsShouldBeSorted()) {
                plugins = plugins.sort(function(a, b) {
                    if(a.name > b.name){ return 1; }
                    if(a.name < b.name){ return -1; }
                    return 0;
                });
            }
            return this.map(plugins, function (p) {
                var mimeTypes = this.map(p, function(mt){
                    return [mt.type, mt.suffixes].join("~");
                }).join(",");
                return [p.name, p.description, mimeTypes].join("::");
            }, this).join(";");
        },
        getIEPluginsString: function () {
            if(window.ActiveXObject){
                var names = [
                    "AcroPDF.PDF", // Adobe PDF reader 7+
                    "Adodb.Stream",
                    "AgControl.AgControl", // Silverlight
                    "DevalVRXCtrl.DevalVRXCtrl.1",
                    "MacromediaFlashPaper.MacromediaFlashPaper",
                    "Msxml2.DOMDocument",
                    "Msxml2.XMLHTTP",
                    "PDF.PdfCtrl", // Adobe PDF reader 6 and earlier, brrr
                    "QuickTime.QuickTime", // QuickTime
                    "QuickTimeCheckObject.QuickTimeCheck.1",
                    "RealPlayer",
                    "RealPlayer.RealPlayer(tm) ActiveX Control (32-bit)",
                    "RealVideo.RealVideo(tm) ActiveX Control (32-bit)",
                    "Scripting.Dictionary",
                    "SWCtl.SWCtl", // ShockWave player
                    "Shell.UIHelper",
                    "ShockwaveFlash.ShockwaveFlash", //flash plugin
                    "Skype.Detection",
                    "TDCCtl.TDCCtl",
                    "WMPlayer.OCX", // Windows media player
                    "rmocx.RealPlayer G2 Control",
                    "rmocx.RealPlayer G2 Control.1"
                ];
                // starting to detect plugins in IE
                return this.map(names, function(name){
                    try{
                        new ActiveXObject(name); // eslint-disable-no-new
                        return name;
                    } catch(e){
                        return null;
                    }
                }).join(";");
            } else {
                return "";
            }
        },
        pluginsShouldBeSorted: function () {
            var should = false;
            for(var i = 0, l = this.options.sortPluginsFor.length; i < l; i++) {
                var re = this.options.sortPluginsFor[i];
                if(navigator.userAgent.match(re)) {
                    should = true;
                    break;
                }
            }
            return should;
        },
        touchSupportKey: function (keys) {
            if(!this.options.excludeTouchSupport){
                keys.push(this.getTouchSupport());
            }
            return keys;
        },
        hasSessionStorage: function () {
            try {
                return !!window.sessionStorage;
            } catch(e) {
                return true; // SecurityError when referencing it means it exists
            }
        },
        // https://bugzilla.mozilla.org/show_bug.cgi?id=781447
        hasLocalStorage: function () {
            try {
                return !!window.localStorage;
            } catch(e) {
                return true; // SecurityError when referencing it means it exists
            }
        },
        hasIndexedDB: function (){
            return !!window.indexedDB;
        },
        getNavigatorCpuClass: function () {
            if(navigator.cpuClass){
                return "navigatorCpuClass: " + navigator.cpuClass;
            } else {
                return "navigatorCpuClass: unknown";
            }
        },
        getNavigatorPlatform: function () {
            if(navigator.platform) {
                return "navigatorPlatform: " + navigator.platform;
            } else {
                return "navigatorPlatform: unknown";
            }
        },
        getDoNotTrack: function () {
            if(navigator.doNotTrack) {
                return "doNotTrack: " + navigator.doNotTrack;
            } else {
                return "doNotTrack: unknown";
            }
        },
        // This is a crude and primitive touch screen detection.
        // It's not possible to currently reliably detect the  availability of a touch screen
        // with a JS, without actually subscribing to a touch event.
        // http://www.stucox.com/blog/you-cant-detect-a-touchscreen/
        // https://github.com/Modernizr/Modernizr/issues/548
        // method returns an array of 3 values:
        // maxTouchPoints, the success or failure of creating a TouchEvent,
        // and the availability of the 'ontouchstart' property
        getTouchSupport: function () {
            var maxTouchPoints = 0;
            var touchEvent = false;
            if(typeof navigator.maxTouchPoints !== "undefined") {
                maxTouchPoints = navigator.maxTouchPoints;
            } else if (typeof navigator.msMaxTouchPoints !== "undefined") {
                maxTouchPoints = navigator.msMaxTouchPoints;
            }
            try {
                document.createEvent("TouchEvent");
                touchEvent = true;
            } catch(_) { /* squelch */ }
            var touchStart = "ontouchstart" in window;
            return [maxTouchPoints, touchEvent, touchStart];
        },
        // https://www.browserleaks.com/canvas#how-does-it-work
        getCanvasFp: function() {
            var result = [];
            // Very simple now, need to make it more complex (geo shapes etc)
            var canvas = document.createElement("canvas");
            canvas.width = 2000;
            canvas.height = 200;
            canvas.style.display = "inline";
            var ctx = canvas.getContext("2d");
            // detect browser support of canvas winding
            // http://blogs.adobe.com/webplatform/2013/01/30/winding-rules-in-canvas/
            // https://github.com/Modernizr/Modernizr/blob/master/feature-detects/canvas/winding.js
            ctx.rect(0, 0, 10, 10);
            ctx.rect(2, 2, 6, 6);
            result.push("canvas winding:" + ((ctx.isPointInPath(5, 5, "evenodd") === false) ? "yes" : "no"));

            ctx.textBaseline = "alphabetic";
            ctx.fillStyle = "#f60";
            ctx.fillRect(125, 1, 62, 20);
            ctx.fillStyle = "#069";
            ctx.font = "11pt no-real-font-123";
            ctx.fillText("Cwm fjordbank glyphs vext quiz, \ud83d\ude03", 2, 15);
            ctx.fillStyle = "rgba(102, 204, 0, 0.7)";
            ctx.font = "18pt Arial";
            ctx.fillText("Cwm fjordbank glyphs vext quiz, \ud83d\ude03", 4, 45);

            // canvas blending
            // http://blogs.adobe.com/webplatform/2013/01/28/blending-features-in-canvas/
            // http://jsfiddle.net/NDYV8/16/
            ctx.globalCompositeOperation = "multiply";
            ctx.fillStyle = "rgb(255,0,255)";
            ctx.beginPath();
            ctx.arc(50, 50, 50, 0, Math.PI * 2, true);
            ctx.closePath();
            ctx.fill();
            ctx.fillStyle = "rgb(0,255,255)";
            ctx.beginPath();
            ctx.arc(100, 50, 50, 0, Math.PI * 2, true);
            ctx.closePath();
            ctx.fill();
            ctx.fillStyle = "rgb(255,255,0)";
            ctx.beginPath();
            ctx.arc(75, 100, 50, 0, Math.PI * 2, true);
            ctx.closePath();
            ctx.fill();
            ctx.fillStyle = "rgb(255,0,255)";
            // canvas winding
            // http://blogs.adobe.com/webplatform/2013/01/30/winding-rules-in-canvas/
            // http://jsfiddle.net/NDYV8/19/
            ctx.arc(75, 75, 75, 0, Math.PI * 2, true);
            ctx.arc(75, 75, 25, 0, Math.PI * 2, true);
            ctx.fill("evenodd");

            result.push("canvas fp:" + canvas.toDataURL());
            return result.join("~");
        },

        getWebglFp: function() {
            var gl;
            var fa2s = function(fa) {
                gl.clearColor(0.0, 0.0, 0.0, 1.0);
                gl.enable(gl.DEPTH_TEST);
                gl.depthFunc(gl.LEQUAL);
                gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
                return "[" + fa[0] + ", " + fa[1] + "]";
            };
            var maxAnisotropy = function(gl) {
                var anisotropy, ext = gl.getExtension("EXT_texture_filter_anisotropic") || gl.getExtension("WEBKIT_EXT_texture_filter_anisotropic") || gl.getExtension("MOZ_EXT_texture_filter_anisotropic");
                return ext ? (anisotropy = gl.getParameter(ext.MAX_TEXTURE_MAX_ANISOTROPY_EXT), 0 === anisotropy && (anisotropy = 2), anisotropy) : null;
            };
            gl = this.getWebglCanvas();
            if(!gl) { return null; }
            // WebGL fingerprinting is a combination of techniques, found in MaxMind antifraud script & Augur fingerprinting.
            // First it draws a gradient object with shaders and convers the image to the Base64 string.
            // Then it enumerates all WebGL extensions & capabilities and appends them to the Base64 string, resulting in a huge WebGL string, potentially very unique on each device
            // Since iOS supports webgl starting from version 8.1 and 8.1 runs on several graphics chips, the results may be different across ios devices, but we need to verify it.
            var result = [];
            var vShaderTemplate = "attribute vec2 attrVertex;varying vec2 varyinTexCoordinate;uniform vec2 uniformOffset;void main(){varyinTexCoordinate=attrVertex+uniformOffset;gl_Position=vec4(attrVertex,0,1);}";
            var fShaderTemplate = "precision mediump float;varying vec2 varyinTexCoordinate;void main() {gl_FragColor=vec4(varyinTexCoordinate,0,1);}";
            var vertexPosBuffer = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, vertexPosBuffer);
            var vertices = new Float32Array([-.2, -.9, 0, .4, -.26, 0, 0, .732134444, 0]);
            gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
            vertexPosBuffer.itemSize = 3;
            vertexPosBuffer.numItems = 3;
            var program = gl.createProgram(), vshader = gl.createShader(gl.VERTEX_SHADER);
            gl.shaderSource(vshader, vShaderTemplate);
            gl.compileShader(vshader);
            var fshader = gl.createShader(gl.FRAGMENT_SHADER);
            gl.shaderSource(fshader, fShaderTemplate);
            gl.compileShader(fshader);
            gl.attachShader(program, vshader);
            gl.attachShader(program, fshader);
            gl.linkProgram(program);
            gl.useProgram(program);
            program.vertexPosAttrib = gl.getAttribLocation(program, "attrVertex");
            program.offsetUniform = gl.getUniformLocation(program, "uniformOffset");
            gl.enableVertexAttribArray(program.vertexPosArray);
            gl.vertexAttribPointer(program.vertexPosAttrib, vertexPosBuffer.itemSize, gl.FLOAT, !1, 0, 0);
            gl.uniform2f(program.offsetUniform, 1, 1);
            gl.drawArrays(gl.TRIANGLE_STRIP, 0, vertexPosBuffer.numItems);
            if (gl.canvas != null) { result.push(gl.canvas.toDataURL()); }
            result.push("extensions:" + gl.getSupportedExtensions().join(";"));
            result.push("webgl aliased line width range:" + fa2s(gl.getParameter(gl.ALIASED_LINE_WIDTH_RANGE)));
            result.push("webgl aliased point size range:" + fa2s(gl.getParameter(gl.ALIASED_POINT_SIZE_RANGE)));
            result.push("webgl alpha bits:" + gl.getParameter(gl.ALPHA_BITS));
            result.push("webgl antialiasing:" + (gl.getContextAttributes().antialias ? "yes" : "no"));
            result.push("webgl blue bits:" + gl.getParameter(gl.BLUE_BITS));
            result.push("webgl depth bits:" + gl.getParameter(gl.DEPTH_BITS));
            result.push("webgl green bits:" + gl.getParameter(gl.GREEN_BITS));
            result.push("webgl max anisotropy:" + maxAnisotropy(gl));
            result.push("webgl max combined texture image units:" + gl.getParameter(gl.MAX_COMBINED_TEXTURE_IMAGE_UNITS));
            result.push("webgl max cube map texture size:" + gl.getParameter(gl.MAX_CUBE_MAP_TEXTURE_SIZE));
            result.push("webgl max fragment uniform vectors:" + gl.getParameter(gl.MAX_FRAGMENT_UNIFORM_VECTORS));
            result.push("webgl max render buffer size:" + gl.getParameter(gl.MAX_RENDERBUFFER_SIZE));
            result.push("webgl max texture image units:" + gl.getParameter(gl.MAX_TEXTURE_IMAGE_UNITS));
            result.push("webgl max texture size:" + gl.getParameter(gl.MAX_TEXTURE_SIZE));
            result.push("webgl max varying vectors:" + gl.getParameter(gl.MAX_VARYING_VECTORS));
            result.push("webgl max vertex attribs:" + gl.getParameter(gl.MAX_VERTEX_ATTRIBS));
            result.push("webgl max vertex texture image units:" + gl.getParameter(gl.MAX_VERTEX_TEXTURE_IMAGE_UNITS));
            result.push("webgl max vertex uniform vectors:" + gl.getParameter(gl.MAX_VERTEX_UNIFORM_VECTORS));
            result.push("webgl max viewport dims:" + fa2s(gl.getParameter(gl.MAX_VIEWPORT_DIMS)));
            result.push("webgl red bits:" + gl.getParameter(gl.RED_BITS));
            result.push("webgl renderer:" + gl.getParameter(gl.RENDERER));
            result.push("webgl shading language version:" + gl.getParameter(gl.SHADING_LANGUAGE_VERSION));
            result.push("webgl stencil bits:" + gl.getParameter(gl.STENCIL_BITS));
            result.push("webgl vendor:" + gl.getParameter(gl.VENDOR));
            result.push("webgl version:" + gl.getParameter(gl.VERSION));

            if (!gl.getShaderPrecisionFormat) {
                if (typeof NODEBUG === "undefined") {
                    this.log("WebGL fingerprinting is incomplete, because your browser does not support getShaderPrecisionFormat");
                }
                return result.join("~");
            }

            result.push("webgl vertex shader high float precision:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.HIGH_FLOAT ).precision);
            result.push("webgl vertex shader high float precision rangeMin:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.HIGH_FLOAT ).rangeMin);
            result.push("webgl vertex shader high float precision rangeMax:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.HIGH_FLOAT ).rangeMax);
            result.push("webgl vertex shader medium float precision:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.MEDIUM_FLOAT ).precision);
            result.push("webgl vertex shader medium float precision rangeMin:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.MEDIUM_FLOAT ).rangeMin);
            result.push("webgl vertex shader medium float precision rangeMax:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.MEDIUM_FLOAT ).rangeMax);
            result.push("webgl vertex shader low float precision:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.LOW_FLOAT ).precision);
            result.push("webgl vertex shader low float precision rangeMin:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.LOW_FLOAT ).rangeMin);
            result.push("webgl vertex shader low float precision rangeMax:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.LOW_FLOAT ).rangeMax);
            result.push("webgl fragment shader high float precision:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.HIGH_FLOAT ).precision);
            result.push("webgl fragment shader high float precision rangeMin:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.HIGH_FLOAT ).rangeMin);
            result.push("webgl fragment shader high float precision rangeMax:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.HIGH_FLOAT ).rangeMax);
            result.push("webgl fragment shader medium float precision:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.MEDIUM_FLOAT ).precision);
            result.push("webgl fragment shader medium float precision rangeMin:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.MEDIUM_FLOAT ).rangeMin);
            result.push("webgl fragment shader medium float precision rangeMax:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.MEDIUM_FLOAT ).rangeMax);
            result.push("webgl fragment shader low float precision:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.LOW_FLOAT ).precision);
            result.push("webgl fragment shader low float precision rangeMin:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.LOW_FLOAT ).rangeMin);
            result.push("webgl fragment shader low float precision rangeMax:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.LOW_FLOAT ).rangeMax);
            result.push("webgl vertex shader high int precision:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.HIGH_INT ).precision);
            result.push("webgl vertex shader high int precision rangeMin:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.HIGH_INT ).rangeMin);
            result.push("webgl vertex shader high int precision rangeMax:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.HIGH_INT ).rangeMax);
            result.push("webgl vertex shader medium int precision:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.MEDIUM_INT ).precision);
            result.push("webgl vertex shader medium int precision rangeMin:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.MEDIUM_INT ).rangeMin);
            result.push("webgl vertex shader medium int precision rangeMax:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.MEDIUM_INT ).rangeMax);
            result.push("webgl vertex shader low int precision:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.LOW_INT ).precision);
            result.push("webgl vertex shader low int precision rangeMin:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.LOW_INT ).rangeMin);
            result.push("webgl vertex shader low int precision rangeMax:" + gl.getShaderPrecisionFormat(gl.VERTEX_SHADER, gl.LOW_INT ).rangeMax);
            result.push("webgl fragment shader high int precision:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.HIGH_INT ).precision);
            result.push("webgl fragment shader high int precision rangeMin:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.HIGH_INT ).rangeMin);
            result.push("webgl fragment shader high int precision rangeMax:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.HIGH_INT ).rangeMax);
            result.push("webgl fragment shader medium int precision:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.MEDIUM_INT ).precision);
            result.push("webgl fragment shader medium int precision rangeMin:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.MEDIUM_INT ).rangeMin);
            result.push("webgl fragment shader medium int precision rangeMax:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.MEDIUM_INT ).rangeMax);
            result.push("webgl fragment shader low int precision:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.LOW_INT ).precision);
            result.push("webgl fragment shader low int precision rangeMin:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.LOW_INT ).rangeMin);
            result.push("webgl fragment shader low int precision rangeMax:" + gl.getShaderPrecisionFormat(gl.FRAGMENT_SHADER, gl.LOW_INT ).rangeMax);
            return result.join("~");
        },
        getAdBlock: function(){
            var ads = document.createElement("div");
            ads.setAttribute("id", "ads");
            document.body.appendChild(ads);
            return document.getElementById("ads") ? false : true;
        },
        getHasLiedLanguages: function(){
            //We check if navigator.language is equal to the first language of navigator.languages
            if(typeof navigator.languages !== "undefined"){
                try {
                    var firstLanguages = navigator.languages[0].substr(0, 2);
                    if(firstLanguages !== navigator.language.substr(0, 2)){
                        return true;
                    }
                } catch(err){
                    return true;
                }
            }
            return false;
        },
        getHasLiedResolution: function(){
            if(screen.width < screen.availWidth){
                return true;
            }
            if(screen.height < screen.availHeight){
                return true;
            }
            return false;
        },
        getHasLiedOs: function(){
            var userAgent = navigator.userAgent;
            var oscpu = navigator.oscpu;
            var platform = navigator.platform;
            var os;
            //We extract the OS from the user agent (respect the order of the if else if statement)
            if(userAgent.toLowerCase().indexOf("windows phone") >= 0){
                os = "Windows Phone";
            } else if(userAgent.toLowerCase().indexOf("win") >= 0){
                os = "Windows";
            } else if(userAgent.toLowerCase().indexOf("android") >= 0){
                os = "Android";
            } else if(userAgent.toLowerCase().indexOf("linux") >= 0){
                os = "Linux";
            } else if(userAgent.toLowerCase().indexOf("iPhone") >= 0 || userAgent.toLowerCase().indexOf("iPad") >= 0 ){
                os = "iOS";
            } else if(userAgent.toLowerCase().indexOf("mac") >= 0){
                os = "Mac";
            } else{
                os = "Other";
            }
            // We detect if the person uses a mobile device
            var mobileDevice;
            if (("ontouchstart" in window) ||
                (navigator.maxTouchPoints > 0) ||
                (navigator.msMaxTouchPoints > 0)) {
                mobileDevice = true;
            } else{
                mobileDevice = false;
            }

            if(mobileDevice && os !== "Windows Phone" && os !== "Android" && os !== "iOS" && os !== "Other"){
                return true;
            }

            // We compare oscpu with the OS extracted from the UA
            if(typeof oscpu !== "undefined"){
                if(oscpu.toLowerCase().indexOf("win") >= 0 && os !== "Windows" && os !== "Windows Phone"){
                    return true;
                } else if(oscpu.toLowerCase().indexOf("linux") >= 0 && os !== "Linux" && os !== "Android"){
                    return true;
                } else if(oscpu.toLowerCase().indexOf("mac") >= 0 && os !== "Mac" && os !== "iOS"){
                    return true;
                } else if(oscpu.toLowerCase().indexOf("win") === 0 && oscpu.toLowerCase().indexOf("linux") === 0 && oscpu.toLowerCase().indexOf("mac") >= 0 && os !== "other"){
                    return true;
                }
            }

            //We compare platform with the OS extracted from the UA
            if(platform.toLowerCase().indexOf("win") >= 0 && os !== "Windows" && os !== "Windows Phone"){
                return true;
            } else if((platform.toLowerCase().indexOf("linux") >= 0 || platform.toLowerCase().indexOf("android") >= 0 || platform.toLowerCase().indexOf("pike") >= 0) && os !== "Linux" && os !== "Android"){
                return true;
            } else if((platform.toLowerCase().indexOf("mac") >= 0 || platform.toLowerCase().indexOf("ipad") >= 0 || platform.toLowerCase().indexOf("ipod") >= 0 || platform.toLowerCase().indexOf("iphone") >= 0) && os !== "Mac" && os !== "iOS"){
                return true;
            } else if(platform.toLowerCase().indexOf("win") === 0 && platform.toLowerCase().indexOf("linux") === 0 && platform.toLowerCase().indexOf("mac") >= 0 && os !== "other"){
                return true;
            }

            if(typeof navigator.plugins === "undefined" && os !== "Windows" && os !== "Windows Phone"){
                //We are are in the case where the person uses ie, therefore we can infer that it's windows
                return true;
            }

            return false;
        },
        getHasLiedBrowser: function () {
            var userAgent = navigator.userAgent;
            var productSub = navigator.productSub;

            //we extract the browser from the user agent (respect the order of the tests)
            var browser;
            if(userAgent.toLowerCase().indexOf("firefox") >= 0){
                browser = "Firefox";
            } else if(userAgent.toLowerCase().indexOf("opera") >= 0 || userAgent.toLowerCase().indexOf("opr") >= 0){
                browser = "Opera";
            } else if(userAgent.toLowerCase().indexOf("chrome") >= 0){
                browser = "Chrome";
            } else if(userAgent.toLowerCase().indexOf("safari") >= 0){
                browser = "Safari";
            } else if(userAgent.toLowerCase().indexOf("trident") >= 0){
                browser = "Internet Explorer";
            } else{
                browser = "Other";
            }

            if((browser === "Chrome" || browser === "Safari" || browser === "Opera") && productSub !== "20030107"){
                return true;
            }

            var tempRes = eval.toString().length;
            if(tempRes === 37 && browser !== "Safari" && browser !== "Firefox" && browser !== "Other"){
                return true;
            } else if(tempRes === 39 && browser !== "Internet Explorer" && browser !== "Other"){
                return true;
            } else if(tempRes === 33 && browser !== "Chrome" && browser !== "Opera" && browser !== "Other"){
                return true;
            }

            //We create an error to see how it is handled
            var errFirefox;
            try {
                throw "a";
            } catch(err){
                try{
                    err.toSource();
                    errFirefox = true;
                } catch(errOfErr){
                    errFirefox = false;
                }
            }
            if(errFirefox && browser !== "Firefox" && browser !== "Other"){
                return true;
            }
            return false;
        },
        isCanvasSupported: function () {
            var elem = document.createElement("canvas");
            return !!(elem.getContext && elem.getContext("2d"));
        },
        isWebGlSupported: function() {
            // code taken from Modernizr
            if (!this.isCanvasSupported()) {
                return false;
            }

            var canvas = document.createElement("canvas"),
                glContext;

            try {
                glContext = canvas.getContext && (canvas.getContext("webgl") || canvas.getContext("experimental-webgl"));
            } catch(e) {
                glContext = false;
            }

            return !!window.WebGLRenderingContext && !!glContext;
        },
        isIE: function () {
            if(navigator.appName === "Microsoft Internet Explorer") {
                return true;
            } else if(navigator.appName === "Netscape" && /Trident/.test(navigator.userAgent)) { // IE 11
                return true;
            }
            return false;
        },
        hasSwfObjectLoaded: function(){
            return typeof window.swfobject !== "undefined";
        },
        hasMinFlashInstalled: function () {
            return swfobject.hasFlashPlayerVersion("9.0.0");
        },
        addFlashDivNode: function() {
            var node = document.createElement("div");
            node.setAttribute("id", this.options.swfContainerId);
            document.body.appendChild(node);
        },
        loadSwfAndDetectFonts: function(done) {
            var hiddenCallback = "___fp_swf_loaded";
            window[hiddenCallback] = function(fonts) {
                done(fonts);
            };
            var id = this.options.swfContainerId;
            this.addFlashDivNode();
            var flashvars = { onReady: hiddenCallback};
            var flashparams = { allowScriptAccess: "always", menu: "false" };
            swfobject.embedSWF(this.options.swfPath, id, "1", "1", "9.0.0", false, flashvars, flashparams, {});
        },
        getWebglCanvas: function() {
            var canvas = document.createElement("canvas");
            var gl = null;
            try {
                gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
            } catch(e) { /* squelch */ }
            if (!gl) { gl = null; }
            return gl;
        },
        each: function (obj, iterator, context) {
            if (obj === null) {
                return;
            }
            if (this.nativeForEach && obj.forEach === this.nativeForEach) {
                obj.forEach(iterator, context);
            } else if (obj.length === +obj.length) {
                for (var i = 0, l = obj.length; i < l; i++) {
                    if (iterator.call(context, obj[i], i, obj) === {}) { return; }
                }
            } else {
                for (var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        if (iterator.call(context, obj[key], key, obj) === {}) { return; }
                    }
                }
            }
        },

        map: function(obj, iterator, context) {
            var results = [];
            // Not using strict equality so that this acts as a
            // shortcut to checking for `null` and `undefined`.
            if (obj == null) { return results; }
            if (this.nativeMap && obj.map === this.nativeMap) { return obj.map(iterator, context); }
            this.each(obj, function(value, index, list) {
                results[results.length] = iterator.call(context, value, index, list);
            });
            return results;
        },

        /// MurmurHash3 related functions

        //
        // Given two 64bit ints (as an array of two 32bit ints) returns the two
        // added together as a 64bit int (as an array of two 32bit ints).
        //
        x64Add: function(m, n) {
            m = [m[0] >>> 16, m[0] & 0xffff, m[1] >>> 16, m[1] & 0xffff];
            n = [n[0] >>> 16, n[0] & 0xffff, n[1] >>> 16, n[1] & 0xffff];
            var o = [0, 0, 0, 0];
            o[3] += m[3] + n[3];
            o[2] += o[3] >>> 16;
            o[3] &= 0xffff;
            o[2] += m[2] + n[2];
            o[1] += o[2] >>> 16;
            o[2] &= 0xffff;
            o[1] += m[1] + n[1];
            o[0] += o[1] >>> 16;
            o[1] &= 0xffff;
            o[0] += m[0] + n[0];
            o[0] &= 0xffff;
            return [(o[0] << 16) | o[1], (o[2] << 16) | o[3]];
        },

        //
        // Given two 64bit ints (as an array of two 32bit ints) returns the two
        // multiplied together as a 64bit int (as an array of two 32bit ints).
        //
        x64Multiply: function(m, n) {
            m = [m[0] >>> 16, m[0] & 0xffff, m[1] >>> 16, m[1] & 0xffff];
            n = [n[0] >>> 16, n[0] & 0xffff, n[1] >>> 16, n[1] & 0xffff];
            var o = [0, 0, 0, 0];
            o[3] += m[3] * n[3];
            o[2] += o[3] >>> 16;
            o[3] &= 0xffff;
            o[2] += m[2] * n[3];
            o[1] += o[2] >>> 16;
            o[2] &= 0xffff;
            o[2] += m[3] * n[2];
            o[1] += o[2] >>> 16;
            o[2] &= 0xffff;
            o[1] += m[1] * n[3];
            o[0] += o[1] >>> 16;
            o[1] &= 0xffff;
            o[1] += m[2] * n[2];
            o[0] += o[1] >>> 16;
            o[1] &= 0xffff;
            o[1] += m[3] * n[1];
            o[0] += o[1] >>> 16;
            o[1] &= 0xffff;
            o[0] += (m[0] * n[3]) + (m[1] * n[2]) + (m[2] * n[1]) + (m[3] * n[0]);
            o[0] &= 0xffff;
            return [(o[0] << 16) | o[1], (o[2] << 16) | o[3]];
        },
        //
        // Given a 64bit int (as an array of two 32bit ints) and an int
        // representing a number of bit positions, returns the 64bit int (as an
        // array of two 32bit ints) rotated left by that number of positions.
        //
        x64Rotl: function(m, n) {
            n %= 64;
            if (n === 32) {
                return [m[1], m[0]];
            }
            else if (n < 32) {
                return [(m[0] << n) | (m[1] >>> (32 - n)), (m[1] << n) | (m[0] >>> (32 - n))];
            }
            else {
                n -= 32;
                return [(m[1] << n) | (m[0] >>> (32 - n)), (m[0] << n) | (m[1] >>> (32 - n))];
            }
        },
        //
        // Given a 64bit int (as an array of two 32bit ints) and an int
        // representing a number of bit positions, returns the 64bit int (as an
        // array of two 32bit ints) shifted left by that number of positions.
        //
        x64LeftShift: function(m, n) {
            n %= 64;
            if (n === 0) {
                return m;
            }
            else if (n < 32) {
                return [(m[0] << n) | (m[1] >>> (32 - n)), m[1] << n];
            }
            else {
                return [m[1] << (n - 32), 0];
            }
        },
        //
        // Given two 64bit ints (as an array of two 32bit ints) returns the two
        // xored together as a 64bit int (as an array of two 32bit ints).
        //
        x64Xor: function(m, n) {
            return [m[0] ^ n[0], m[1] ^ n[1]];
        },
        //
        // Given a block, returns murmurHash3's final x64 mix of that block.
        // (`[0, h[0] >>> 1]` is a 33 bit unsigned right shift. This is the
        // only place where we need to right shift 64bit ints.)
        //
        x64Fmix: function(h) {
            h = this.x64Xor(h, [0, h[0] >>> 1]);
            h = this.x64Multiply(h, [0xff51afd7, 0xed558ccd]);
            h = this.x64Xor(h, [0, h[0] >>> 1]);
            h = this.x64Multiply(h, [0xc4ceb9fe, 0x1a85ec53]);
            h = this.x64Xor(h, [0, h[0] >>> 1]);
            return h;
        },

        //
        // Given a string and an optional seed as an int, returns a 128 bit
        // hash using the x64 flavor of MurmurHash3, as an unsigned hex.
        //
        x64hash128: function (key, seed) {
            key = key || "";
            seed = seed || 0;
            var remainder = key.length % 16;
            var bytes = key.length - remainder;
            var h1 = [0, seed];
            var h2 = [0, seed];
            var k1 = [0, 0];
            var k2 = [0, 0];
            var c1 = [0x87c37b91, 0x114253d5];
            var c2 = [0x4cf5ad43, 0x2745937f];
            for (var i = 0; i < bytes; i = i + 16) {
                k1 = [((key.charCodeAt(i + 4) & 0xff)) | ((key.charCodeAt(i + 5) & 0xff) << 8) | ((key.charCodeAt(i + 6) & 0xff) << 16) | ((key.charCodeAt(i + 7) & 0xff) << 24), ((key.charCodeAt(i) & 0xff)) | ((key.charCodeAt(i + 1) & 0xff) << 8) | ((key.charCodeAt(i + 2) & 0xff) << 16) | ((key.charCodeAt(i + 3) & 0xff) << 24)];
                k2 = [((key.charCodeAt(i + 12) & 0xff)) | ((key.charCodeAt(i + 13) & 0xff) << 8) | ((key.charCodeAt(i + 14) & 0xff) << 16) | ((key.charCodeAt(i + 15) & 0xff) << 24), ((key.charCodeAt(i + 8) & 0xff)) | ((key.charCodeAt(i + 9) & 0xff) << 8) | ((key.charCodeAt(i + 10) & 0xff) << 16) | ((key.charCodeAt(i + 11) & 0xff) << 24)];
                k1 = this.x64Multiply(k1, c1);
                k1 = this.x64Rotl(k1, 31);
                k1 = this.x64Multiply(k1, c2);
                h1 = this.x64Xor(h1, k1);
                h1 = this.x64Rotl(h1, 27);
                h1 = this.x64Add(h1, h2);
                h1 = this.x64Add(this.x64Multiply(h1, [0, 5]), [0, 0x52dce729]);
                k2 = this.x64Multiply(k2, c2);
                k2 = this.x64Rotl(k2, 33);
                k2 = this.x64Multiply(k2, c1);
                h2 = this.x64Xor(h2, k2);
                h2 = this.x64Rotl(h2, 31);
                h2 = this.x64Add(h2, h1);
                h2 = this.x64Add(this.x64Multiply(h2, [0, 5]), [0, 0x38495ab5]);
            }
            k1 = [0, 0];
            k2 = [0, 0];
            switch(remainder) {
                case 15:
                    k2 = this.x64Xor(k2, this.x64LeftShift([0, key.charCodeAt(i + 14)], 48));
                case 14:
                    k2 = this.x64Xor(k2, this.x64LeftShift([0, key.charCodeAt(i + 13)], 40));
                case 13:
                    k2 = this.x64Xor(k2, this.x64LeftShift([0, key.charCodeAt(i + 12)], 32));
                case 12:
                    k2 = this.x64Xor(k2, this.x64LeftShift([0, key.charCodeAt(i + 11)], 24));
                case 11:
                    k2 = this.x64Xor(k2, this.x64LeftShift([0, key.charCodeAt(i + 10)], 16));
                case 10:
                    k2 = this.x64Xor(k2, this.x64LeftShift([0, key.charCodeAt(i + 9)], 8));
                case 9:
                    k2 = this.x64Xor(k2, [0, key.charCodeAt(i + 8)]);
                    k2 = this.x64Multiply(k2, c2);
                    k2 = this.x64Rotl(k2, 33);
                    k2 = this.x64Multiply(k2, c1);
                    h2 = this.x64Xor(h2, k2);
                case 8:
                    k1 = this.x64Xor(k1, this.x64LeftShift([0, key.charCodeAt(i + 7)], 56));
                case 7:
                    k1 = this.x64Xor(k1, this.x64LeftShift([0, key.charCodeAt(i + 6)], 48));
                case 6:
                    k1 = this.x64Xor(k1, this.x64LeftShift([0, key.charCodeAt(i + 5)], 40));
                case 5:
                    k1 = this.x64Xor(k1, this.x64LeftShift([0, key.charCodeAt(i + 4)], 32));
                case 4:
                    k1 = this.x64Xor(k1, this.x64LeftShift([0, key.charCodeAt(i + 3)], 24));
                case 3:
                    k1 = this.x64Xor(k1, this.x64LeftShift([0, key.charCodeAt(i + 2)], 16));
                case 2:
                    k1 = this.x64Xor(k1, this.x64LeftShift([0, key.charCodeAt(i + 1)], 8));
                case 1:
                    k1 = this.x64Xor(k1, [0, key.charCodeAt(i)]);
                    k1 = this.x64Multiply(k1, c1);
                    k1 = this.x64Rotl(k1, 31);
                    k1 = this.x64Multiply(k1, c2);
                    h1 = this.x64Xor(h1, k1);
            }
            h1 = this.x64Xor(h1, [0, key.length]);
            h2 = this.x64Xor(h2, [0, key.length]);
            h1 = this.x64Add(h1, h2);
            h2 = this.x64Add(h2, h1);
            h1 = this.x64Fmix(h1);
            h2 = this.x64Fmix(h2);
            h1 = this.x64Add(h1, h2);
            h2 = this.x64Add(h2, h1);
            return ("00000000" + (h1[0] >>> 0).toString(16)).slice(-8) + ("00000000" + (h1[1] >>> 0).toString(16)).slice(-8) + ("00000000" + (h2[0] >>> 0).toString(16)).slice(-8) + ("00000000" + (h2[1] >>> 0).toString(16)).slice(-8);
        }
    };
    Fingerprint2.VERSION = "0.7.1";
    return Fingerprint2;
});

(function (adsbymobo) {
    'use strict';

    adsbymobo.init = function(adsElement, done){
        var moboURL = "http://tracking.ad.mobo.vn/init-imp";
        var type =  adsElement.getAttribute("type") || 'BANNER';
        var mode =  adsElement.getAttribute("mode") || '';
        var ad_unit_id = adsElement.getAttribute("ad_unit_id");
        var moboParams = {
            mode: 1,
            ad_unit_id: "unknown",
            fpid: "unknown",
            platform: "unknown",
            private_ip: "unknown",
            version: "unknown",
            version_id: "unknown",
            device_type: "unknown",
            screen_size: "unknown",
            language: "unknown",
            longitude: "unknown",
            latitude: "unknown",
            device_model: "unknown"
        };

        sAsync.doSeries([
            getDefine,
            getScreensize,
            getLanguage,
            getOs,
            getFPID,
            getGeo,
            getIPs
        ], function end(result) {
            var url = moboURL + "?" + prepareParams(moboParams);
            //console.log('PARAMS: ', moboParams);
            //console.log('URL: ', url);

            sendXmlHttpRequest(url, function(err, result){

                if(!err){
                    try{
                        var result = JSON.parse(result);
                        if(result.code == 1){
                            var ifm = showIframe(type, ad_unit_id, result.data.banner_url);
                            done(ifm);
                        }
                    }
                    catch(e){
                        console.log("Failed JSON parse", e);
                    }
                }
            });


        });

        //convert JSON object to query params
        function prepareParams(params){
            var str = []
            for(var i in params){
                str.push(i+"="+encodeURIComponent(params[i]));
            }
            return str.join("&");
        }

        //append iframe
        function showIframe(type, ad_unit_id,  href){
            var html = '';

            switch(type){
                case 'BANNER':
                    html = '<iframe id="'+ad_unit_id+'" src="'+href+'" scrolling="no" align="center" frameborder="0" style="width: 320px; height: 50px; margin:0px; border: 0px; padding: 0px;"></iframe>';
                    break;
                case 'POPUP':
                    html = '<iframe id="'+ad_unit_id+'" src="'+href+'" scrolling="no" align="center" frameborder="0" style="width: 100%; height: 100%; margin-left: auto; margin-right: auto; overflow: hidden; position: fixed; left: 0px; right: 0px; bottom: 0px; border: 0px; padding: 0px; z-index: 999999999;"></iframe>';
                    break;
            }

            return html;
        }

        function sendXmlHttpRequest(url, callback){
            try {
                var xhr = window.XMLHttpRequest
                    ? new window.XMLHttpRequest()
                    : window.ActiveXObject
                    ? new ActiveXObject('Microsoft.XMLHTTP')
                    : null;

                xhr.open('GET', url, true);

                // fallback on error
                xhr.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status >= 200 && this.status < 300) {
                        if (typeof callback === 'function') { callback(false, xhr.responseText); }
                    } else if (this.readyState === 4) {
                        console.log("Failed Server XML HTTP request", this.status);
                        if (typeof callback === 'function') { callback(true, url); }
                    }
                };

                xhr.send();
            } catch (e) {
                // fallback
                console.log("Failed XML HTTP request", e);
                if (typeof callback === 'function') { callback(true, url); }
            }
        }

        //getting define
        function getDefine(next){
            moboParams.ad_unit_id = ad_unit_id;
            if(mode == 'TEST_SAND_BOX'){
                moboParams.mode = 0;
            }

            next();
        }

        //getting resolution
        function getScreensize(next){
            if (screen.width) {
                var width = (screen.width) ? screen.width : '';
                var height = (screen.height) ? screen.height : '';
                moboParams.screen_size = '' + height + "." + width;
            }

            next();
        }

        //getting language
        function getLanguage(next) {
            var language = navigator.language || navigator.browserLanguage || navigator.systemLanguage || navigator.userLanguage || "unknown";
            if (typeof language !== "undefined")
                moboParams.language = language;

            next();
        }

        //getting os
        function getOs(next) {
            var nAgt = navigator.userAgent,
                nVer = navigator.appVersion,
                clientStrings = [
                    {s: 'Windows 3.11', r: /Win16/},
                    {s: 'Windows 95', r: /(Windows 95|Win95|Windows_95)/},
                    {s: 'Windows ME', r: /(Win 9x 4.90|Windows ME)/},
                    {s: 'Windows 98', r: /(Windows 98|Win98)/},
                    {s: 'Windows CE', r: /Windows CE/},
                    {s: 'Windows 2000', r: /(Windows NT 5.0|Windows 2000)/},
                    {s: 'Windows XP', r: /(Windows NT 5.1|Windows XP)/},
                    {s: 'Windows Server 2003', r: /Windows NT 5.2/},
                    {s: 'Windows Vista', r: /Windows NT 6.0/},
                    {s: 'Windows 7', r: /(Windows 7|Windows NT 6.1)/},
                    {s: 'Windows 8.1', r: /(Windows 8.1|Windows NT 6.3)/},
                    {s: 'Windows 8', r: /(Windows 8|Windows NT 6.2)/},
                    {s: 'Windows NT 4.0', r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/},
                    {s: 'Windows ME', r: /Windows ME/},
                    {s: 'Windows Phone', r: /Windows Phone/},
                    {s: 'Android', r: /Android/},
                    {s: 'Open BSD', r: /OpenBSD/},
                    {s: 'Sun OS', r: /SunOS/},
                    {s: 'Linux', r: /(Linux|X11)/},
                    {s: 'iOS', r: /(iPhone|iPad|iPod)/},
                    {s: 'Mac OSX', r: /Mac OS X/},
                    {s: 'Mac OS', r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/},
                    {s: 'QNX', r: /QNX/},
                    {s: 'UNIX', r: /UNIX/},
                    {s: 'BeOS', r: /BeOS/},
                    {s: 'OS/2', r: /OS\/2/},
                    {s: 'SearchBot', r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/}
                ];
            for (var id in clientStrings) {
                var cs = clientStrings[id];
                if (cs.r.test(nAgt)) {
                    moboParams.platform = cs.s;
                    break;
                }
            }

            if (/Windows/.test(moboParams.platform) && moboParams.platform != "Windows Phone") {
                moboParams.version = /Windows (.*)/.exec(moboParams.platform)[1];
                moboParams.platform = 'Windows';
            }

            //getting version
            switch (moboParams.platform) {
                case 'Mac OSX':
                    moboParams.version = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                    break;

                case 'Windows Phone':
                    moboParams.version = (/Windows Phone ([\.\_\d]+)/.exec(nAgt) || ["", "8.0"])[1];
                    break;

                case 'Android':
                    moboParams.version = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                    break;

                case 'iOS':
                    moboParams.version = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                    moboParams.version = moboParams.version[1] + '.' + moboParams.version[2] + '.' + (moboParams.version[3] | 0);
                    break;
            }

            //getting version id
            var rex = /Mobile[\/]([a-zA-Z\d]+)/;
            if(rex.test(nAgt)){
                moboParams.version_id = rex.exec(nAgt)[1];
            }

            //getting device type
            clientStrings = [
                {s:'iPhone', r:/(iPhone)/},
                {s:'iPad', r:/(iPad)/},
                {s:'iPod touch', r:/(iPod)/}
            ];
            for (var id in clientStrings) {
                var cs = clientStrings[id];
                if (cs.r.test(nAgt)) {
                    moboParams.device_type = cs.s;
                    break;
                }
            }

            next();
        }

        //getting geolocation
        function getGeo(next){
          moboParams.latitude = "unknown";
          moboParams.longitude = "unknown";
          next();
        }

        //getting private ip
        function getIPs(next){
            function handleCandidate(candidate) {
                //match just the IP address
                var ip_regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/
                var ip_addr = ip_regex.exec(candidate)[1];

                //remove duplicates & match local IPs or IPv6 addresses
                if (ip_dups.indexOf(ip_addr) < 0 && (ip_addr.match(/^(192\.168\.|169\.254\.|10\.|172\.(1[6-9]|2\d|3[01]))/) || ip_addr.match(/^[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7}$/))) {
                    ip_dups.push(ip_addr);
                }

            }

            try{
                var ip_dups = [];

                //compatibility for firefox and chrome
                var RTCPeerConnection = window.RTCPeerConnection
                    || window.mozRTCPeerConnection
                    || window.webkitRTCPeerConnection;
                var useWebKit = !!window.webkitRTCPeerConnection;

                //bypass naive webrtc blocking using an iframe
                if(!RTCPeerConnection){
                    //NOTE: you need to have an iframe in the page right above the script tag
                    //
                    //<iframe id="iframe" sandbox="allow-same-origin" style="display: none"></iframe>
                    //<script>...getIPs called in here...
                    //
                    var body = document.getElementsByTagName("body")[0];
                    var iframe = document.createElement("iframe");
                    iframe.style.display = "none";
                    iframe.setAttribute('sandbox', 'allow-same-origin');

                    var win = iframe.contentWindow;
                    RTCPeerConnection = win.RTCPeerConnection
                    || win.mozRTCPeerConnection
                    || win.webkitRTCPeerConnection;
                    useWebKit = !!win.webkitRTCPeerConnection;
                }

                if(RTCPeerConnection) {
                    //minimal requirements for data connection
                    var mediaConstraints = {
                        optional: [{RtpDataChannels: true}]
                    };

                    var servers = {iceServers: [{urls: "stun:stun.services.mozilla.com"}]};

                    //construct a new RTCPeerConnection
                    var pc = new RTCPeerConnection(servers, mediaConstraints);

                    //listen for candidate events
                    pc.onicecandidate = function (ice) {

                        //skip non-candidate events
                        if (ice.candidate)
                            handleCandidate(ice.candidate.candidate);
                    };

                    //create a bogus data channel
                    pc.createDataChannel("");

                    //create an offer sdp
                    pc.createOffer(function (result) {

                        //trigger the stun server request
                        pc.setLocalDescription(result, function () {
                        }, function () {
                        });

                    }, function () {
                    });

                    //wait for a while to let everything done
                    setTimeout(function () {
                        //read candidate info from local description
                        var lines = pc.localDescription.sdp.split('\n');
                        if (lines) {
                            lines.forEach(function (line) {
                                if (line.indexOf('a=candidate:') === 0)
                                    handleCandidate(line);
                            });
                        }

                        moboParams.private_ip = ip_dups.join('&');
                        next();
                    }, 1000, next);
                }
                else{
                    next();
                }
            }
            catch(e){
                next();
            }
        }

        function getFPID(next){
            var fp = new Fingerprint2();
            fp.get(function(fpid) {
                moboParams.fpid = fpid;
                next();
            });
        }
    }
}(window.adsbymobo = window.adsbymobo || {}));

if(!window.adsbymobo.isload){
    function receiveMessage(event){
        //console.log('EVENT', event);
        if(typeof event.data == 'object'){
            if(event.data.hasOwnProperty("type") && event.data.type == 'closemoboads'){
                var element = document.getElementById(event.data.adUnitId);
                if(element){
                    element.parentNode.removeChild(element);
                }
            }

            if(event.data.hasOwnProperty("type") && event.data.type == 'installmoboads'){
                window.location = decodeURIComponent(event.data.url);
            }
        }
    }
    window.addEventListener("message", receiveMessage, false);

    setTimeout(function(){
        for(var i=0; i<window.adsbymobo.length; i++){
            var adsElement = document.getElementsByClassName("adsbymobo")[i];

            adsbymobo.init(adsElement, function(ifm){
                adsElement.innerHTML = ifm;
            });
        }
    }, 1000);
}
window.adsbymobo.isload = true;
