/**
 * Install.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
  connection: 'MongodbServer',
  schema: true,

  attributes: {
    action: {
      type: 'string',
      defaultsTo: adsAction.INSTALL
    },

    deviceid: {
      type: 'string',
      required: true
    },

    bd_device_id: {
      type: 'string',
    },

    fpid: {
      type: 'string',
    },

    referrer: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    app: {
      type: 'string',
      required: true
    },

    platform: {
      type: 'string',
      required: true
    },

    version: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    version_id: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    public_ip: {
      type: 'ip',
      required: true,
    },

    private_ip: {
      type: 'string',
    },

    dns: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    device_type: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    screen_size: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    language: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    longitude: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    latitude: {
      type: 'string'
    },

    device_model: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    imei: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    manufacturer: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    emails: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    telco: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    }

  },

  afterCreate: function (values, cb) {
    cb();

    // FORMAT MATCHED: utm_source=fpid=wwwwwwwwwwwwwwwww
    // EXAMPLE: utm_source=fpid=b15a959d49f0ce092c4d065ab6baa6f3&u
    var re = new RegExp("fpid=([A-Za-z0-9_]+)&source_type=([A-Za-z0-9_]+)"),
      referrer = values['referrer'];

    if (referrer && re.test(referrer)) {
      var matchStrs = referrer.match(re);

      values['fpid'] = matchStrs[1];
      values['source_type'] = matchStrs[2];
    }

    utils.convertValuesOfObjectIntoLowerCase(values);
    kafka.send(values, kafka.adsTopic);

    // We don't need save this record in db (memory).
    // Install.destroy({id: values.id}).exec(function deleteCB(err){
    //   console.log('The record has been deleted');
    // });
  }
};

