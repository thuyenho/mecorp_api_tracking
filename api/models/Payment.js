/**
* Payment.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  connection: 'MongodbServer',
  schema: true,

  attributes: {

    uid: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    username: {
      type: 'string',
      required: true
    },

    pay_type: {
      type: 'string'
    },

    app: {
      type: 'string',
      required: true
    },

    subapp: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    gateway: {
      type: 'string',
    },

    prefix: {
      type: 'string',
    },

    card_type: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    phone: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    mt: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    mo: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    provider: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    pin: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    seri: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    bank_name: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    money: {
      type: 'integer',
      required: true
    },

    coin: {
      type: 'integer'
    },

    order_id: {
      type: 'string',
    },

    coorder_id: {
      type: 'string',
    },

    order_time: {
      type: 'datetime',
      defaultsTo: new Date()
    },

    coorder_time: {
      type: 'datetime',
      defaultsTo: new Date()
    },

    pay_ip: {
      type: 'ip'
    },

    status: {
      type: 'string',
      required: true,
      in: ['0', '1']
    },

    error: {
      type: 'string',
    }

  },

  afterCreate: function (values, cb) {
    cb();

    utils.convertValuesOfObjectIntoLowerCase(values);
    kafka.send(values, kafka.paymentTopic);

    // // We don't need save this record in db (memory).
    // Payment.destroy({id: values.id}).exec(function deleteCB(err){
    //   //console.log('The record has been deleted');
    // });
  }
};

