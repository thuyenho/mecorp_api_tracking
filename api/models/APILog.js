/**
* APILog.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    accessToken: {
      model: 'AccessToken'
    },

    ip: {
      type: 'string',
      required: true
    },

    calledAPI: {
      type: 'string',
      required: true
    },

    status: {
      type: 'boolean',
      required: true
    }
  }
};

