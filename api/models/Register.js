/**
* Register.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  connection: 'MongodbServer',
  schema: true,

  attributes: {
    deviceid: {
      type: 'string',
      required: true
    },

    uid: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    username: {
      type: 'string',
      required: true
    },

    password: {
      type: 'string'
    },

    user_type: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    email: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    register_time: {
      type: 'datetime',
      defaultsTo: new Date()
    },

    register_ip: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE

    },

    app: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    subapp: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    channel: {
      type: 'string',
    },

    platform: {
      type: 'string',
      required: true
    },

    phone: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    telco: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },
  },

  afterCreate: function (values, cb) {
    cb();

    utils.convertValuesOfObjectIntoLowerCase(values);

    // TODO: we must set timeout beacause register package sometimes send first as soon as  install package is send
    setTimeout(function(){
      kafka.send(values, kafka.registerTopic);

      // // We don't need save this record in db (memory).
      // Register.destroy({id: values.id}).exec(function deleteCB(err){
      //   //console.log('The record has been deleted');
      // });
    }, 30000);
  }
};

