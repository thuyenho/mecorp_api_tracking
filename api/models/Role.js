/**
* Role.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  connection: 'memory',
  schema: true,

  attributes: {
    uid: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    name: {
      type: 'string',
      required: true
    },

    app: {
      type: 'string',
      required: true
    },

    subapp: {
      type: 'string',
      required: true
    },

    created: {
      type: 'datetime',
      required: true
    },

  },

  afterCreate: function (values, cb) {
    cb();
    utils.convertValuesOfObjectIntoLowerCase(values);
    kafka.send(values, kafka.roleTopic);

    // We don't need save this record in db (memory).
    Role.destroy({id: values.id}).exec(function deleteCB(err){
      console.log('The record has been deleted');
    });
  }
};

