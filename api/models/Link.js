/**
* Link.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  connection: 'memory',
  schema: true,

  attributes: {
    action: {
      type: 'string',
      defaultsTo: adsAction.CLICK
    },

    public_ip: {
      type: 'ip',
    },

    publisher: {
      type: 'string',
      required: true
    },

    campaign: {
      type: 'string',
      required: true
    },

    source_type: {
      type: 'integer',
      in: [consts.SOURCE_TYPE_TRACKING_LINK, consts.SOURCE_TYPE_ADS],
      required: true
    },

    fpid: {
      type: 'string',
      required: true
    },

    platform: {
      type: 'string',
      required: true
    },

    version: {
      type: 'string',
    },
  },

  afterCreate: function (values, cb) {
    cb();
    utils.convertValuesOfObjectIntoLowerCase(values);
    kafka.send(values, kafka.trackingLinkTopic);

    // We don't need save this record in db (memory).
    Link.destroy({id: values.id}).exec(function deleteCB(err){
      console.log('The record has been deleted');
    });
  }
};

