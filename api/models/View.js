/**
* View.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  connection: 'memory',
  schema: true,

  attributes: {
    action: {
      type: 'string',
      defaultsTo: adsAction.VIEW
    },

    ip: {
      type: 'ip',
      required: true,
    },

    campaign: {
      type: 'string',
      required: true
    },

    adsgroup: {
      type: 'string',
      required: true
    },

    banner: {
      type: 'string',
      required: true
    },

    zone: {
      type: 'string',
      required: true
    },

    channel: {
      type: 'string',
      required: true
    },

    app: {
      type: 'string',
      required: true
    },

    deviceid: {
      type: 'string',
    },

    fpid: {
      type: 'string',
    },

    platform: {
      type: 'string',
      required: true
    },

    version: {
      type: 'string',
      required: true
    }
  },

  afterValidate: function (values, cb) {
    if (!values.deviceid && !values.fpid) {
      return cb(new Error('Not found device info'));
    }
    return cb();
  },

  afterCreate: function (values, cb) {
    cb();

    utils.convertValuesOfObjectIntoLowerCase(values);
    kafka.send(values, kafka.adsTopic);

    // We don't need save this record in db (memory).
    View.destroy({id: values.id}).exec(function deleteCB(err){
      console.log('The record has been deleted');
    });
  }
};

