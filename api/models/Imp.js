/**
* Imp.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    action: {
      type: 'string',
      defaultsTo: adsAction.IMP
    },

    deviceid: {
      type: 'string',
    },

    fpid: {
      type: 'string',
    },

    platform: {
      type: 'string',
      required: true
    },

    version: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    version_id: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    public_ip: {
      type: 'ip',
      required: true,
    },

    private_ip: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    dns: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    device_type: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    screen_size: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    language: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    longitude: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    latitude: {
      type: 'string'
    },

    device_model: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    ad_unit_id: {
      type: 'string',
      required: true
    },

    pub_campaign: {
      type: 'string',
      required: true
    },

    position: {
      type: 'string',
      in: [consts.AD_TYPE_BANNER, consts.AD_TYPE_POPUP],
      required: true
    },

    banner_image: {
      type: 'string',
      required: true
    },

    re: {
      type: 'string',
      required: true
    }
  },

  afterValidate: function (values, cb) {
    if (!values.deviceid && !values.fpid) {
      return cb(new Error('Not found device info'));
    }
    return cb();
  },

  afterCreate: function (values, cb) {
    cb();
    utils.convertValuesOfObjectIntoLowerCase(values);
    kafka.send(values, kafka.adsTopic);

    // We don't need save this record in db (memory).
    Imp.destroy({id: values.id}).exec(function deleteCB(err){
      console.log('The record has been deleted');
    });
  }
};

