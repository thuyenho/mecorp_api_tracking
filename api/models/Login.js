/**
* Login.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  connection: 'MongodbServer',
  schema: true,

  attributes: {

    uid: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    username: {
      type: 'string',
      required: true
    },

    login_time: {
      type: 'datetime',
      defaultsTo: new Date()
    },

    login_ip: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    app: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },

    subapp: {
      type: 'string',
      defaultsTo: consts.DEFAULT_VALUE
    },
  },

  afterCreate: function (values, cb) {
    cb();

    utils.convertValuesOfObjectIntoLowerCase(values);
    kafka.send(values, kafka.loginTopic);

    // We don't need save this record in db (memory).
    // Login.destroy({id: values.id}).exec(function deleteCB(err){
    //   //console.log('The record has been deleted');
    // });
  }
};

