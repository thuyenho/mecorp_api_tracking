/**
 * PostbackController
 *
 * @description :: Server-side logic for managing postbacks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	find: function (req, res) {
		console.log("-- postback - get --");
		console.log(req.allParams())
    	return res.ok();
  	},

	create: function (req, res) {
		console.log("-- postback - post --");
		console.log(req.allParams())
    	return res.ok();
  	},
	
};

