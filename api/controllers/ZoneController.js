/**
 * ZoneController
 *
 * @description :: Server-side logic for managing zones
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  imp: function (req, res) {
    try {
      var zoneId = req.param('zoneid'),
        deviceId = req.param('deviceid'),
        fpid = req.param('fpid'),
        platform = req.param('platform'),
        position = req.param('position'), // TODO: only for test, please removes this line after test
        version = req.param('version');



      if (!(hasParameter(zoneId) && (hasParameter(deviceId) || hasParameter(fpid)) &&
          hasParameter(platform) && hasParameter(version))) {
        throw new Error (message.NOT_ENOUGH_PARAMS);
      }
    } catch (err) {
      return res.badRequest(err.toString());
    }

    platform = platform.toLowerCase();

    /*
     * TODO: current appstore for platform we are only based on string : 'windowsphone', 'android', 'ios'
     * But some cases platform is different from string above. e.g. 'windows', 'wp', 'ad'
     * So that, we need to build a function to convert other string to correct formated platform.
     * e.g: convert string 'windows' to 'windowsphone'
     */
    if (platform === 'windows' || platform === 'windows phone') {
      platform = 'windowsphone';
    }

    var deviceInfo = {
      platform: platform,
      version: version,
      position: position, // TODO: only for test, please removes this line after test

      // because Api server is behide Nginx Server. we must use param 'x-real-ip' in req header
      ip: req.headers['x-real-ip']
    };

    if (deviceId) {
      deviceInfo.deviceid = deviceId;
    } else {
      deviceInfo.fpid = fpid;
    }

    async.waterfall([

      function(next) {
        findAdsBelongToZone(zoneId, next);
      },

      function(adsObjects, next) {
        assignAppObjectAndBannerObjectToAdsObject(adsObjects, next);
      },

      function(adsObjects, next) {
        filterAdsSupportedForPlatform(adsObjects, platform, next);
      },

      function(adsObjects, next) {
        findAdsMatchCampaignConditions(adsObjects, next);
      },

      function(adsObjects, next) {
        findAdsMatchAdsGroupConditions(adsObjects, next)
      },

      function(adsObjects, next) {
        selectRandomlyCampaignBasedOnBid(adsObjects, next)
      },

      function(adsObjects, next) {
        assignTrafficToAdsObject(adsObjects, next);
      },

      function(adsObjects, next) {
        selectAdsBasedOnTraffic(adsObjects, next);
      },

      function(adsObjects, next) {
        customiseData(adsObjects, deviceInfo, next);
      },

    ], function (err, result) {

      if (err)  {
        res.status(404);
        res.send(err);
      } else {
        //utils.convertValuesOfObjectIntoLowerCase(result);
        sendDataToKafka(result);
        sendDataToClient(result, res);
      }

    });

  },

}

function findAdsBelongToZone(zoneId, next) {

  redis.findKeys('zone' + ':' + zoneId + ':' + 'ads' + ':' + '*',
      function (err, adsKeys) {
        if (adsKeys.length === 0) {
          next(message.NOT_FOUND_ADS);
        } else {
          var adsObjects = [],
            adsIds = getListKeys(adsKeys);

          async.forEach(adsIds, function (adsId, callback) {
            redis.findOne('ads' + ':' + adsId, function (err, adsObject) {
              if (!JSON.parse(adsObject.is_remove)) {
                adsObjects.push(adsObject);
              }
              callback();
            });

          }, function (err) {
            done(null, adsObjects, next);
          });

        }
      }
  );
}

function filterAdsSupportedForPlatform(adsObjects, platform, next) {
  var filteredAds = [],
    i;

  for (i=0, len=adsObjects.length; i < len; i++) {
    var appStores = JSON.parse(adsObjects[i].app.appstore);
    if ((platform in appStores) && appStores[platform]) {
      filteredAds.push(adsObjects[i]);
    }
  }
  done(null, filteredAds, next);
}

function findAdsMatchCampaignConditions(adsObjects, next) {
  var adsMatchConditions = [];

  async.forEach(adsObjects, function(adsObject, callback) {
    redis.findOne('adsgroup' + ':' + adsObject.adsgroup, function (err, adsgroupObject) {

      if (adsgroupObject != null) {
        redis.findOne('campaign' + ':' + adsgroupObject.campaign,
          function (err, campaignObject) {
            if (isInDateRange(campaignObject.from_date, campaignObject.to_date) && parseInt(campaignObject.status) == consts.CAMPAIGN_STATUS_ACTIVE) {
              adsObject.campaign = campaignObject;
              adsMatchConditions.push(adsObject);
            }
            callback();
          });
      } else {
        callback();
      }
    });
  }, function (err) {
    done(err, adsMatchConditions, next);
  });
}

function findAdsMatchAdsGroupConditions(adsObjects, next) {
  var adsMatchConditions = [];

  async.forEach(adsObjects, function(adsObject, callback) {
    redis.findOne('adsgroup' + ':' + adsObject.adsgroup, function (err, adsgroupObject) {

      var runTime = JSON.parse(adsgroupObject.run_time);
      if (isInRuntime(runTime)) {
        adsObject.adsgroup = adsgroupObject;
        adsMatchConditions.push(adsObject);
      }
      callback();
    });

  }, function (err) {
    done(err, adsMatchConditions, next);
  });
}

function selectRandomlyCampaignBasedOnBid(adsObjects, next) {
  var campaigns = getListCampaigns(adsObjects);
  var campaigns = convertPriceOfCpiOfCampaignToPercentage(campaigns);
  var choseCampaign = selectCampaignBasedOnBid(campaigns);

  if (!choseCampaign) {
    done(message.NOT_FOUND_CAMPAIGN, null, next);
  } else {
    adsObjects = findAdsBelongToCampaign(adsObjects, choseCampaign);
    done(null, adsObjects, next);
  }
}

function getListCampaigns(adsObjs) {
  var listOfCampaignIds = [],
    listOfCampaignObjs = [];

  adsObjs.forEach(function(adsObj) {
    if (listOfCampaignIds.indexOf(adsObj.campaign.id) == -1) {
      listOfCampaignIds.push(adsObj.campaign.id);
      listOfCampaignObjs.push(adsObj.campaign);
    }
  })

  return listOfCampaignObjs;
}

function convertPriceOfCpiOfCampaignToPercentage(campaigns) {
  var i, len, totalBid = 0;

  campaigns.forEach(function(obj) {

    var price = parseInt(obj.bid);
    totalBid += price ? price : 0;
  })

  for(i=0, len=campaigns.length; i < len; i++) {
    var bid = parseInt(campaigns[i].bid);
    campaigns[i].bidPercentage = bid ? Math.round((bid/totalBid) * 100) : 0;
  }

  return campaigns;
}

function selectCampaignBasedOnBid(campaigns, next) {
  var sharing = [];
  for (var i = 0; i < campaigns.length; i++) {
    var bidPercentage = parseInt(campaigns[i].bidPercentage);
    for (var j = 0; j < bidPercentage; j++) {
      sharing.push(i);
    }
  }

  var index = getRandomArbitrary(0, sharing.length);
  var choseCampaign = campaigns[sharing[index]];

  if (choseCampaign == undefined) {
    return null;
  } else {
    return choseCampaign;
  }
}


function findAdsBelongToCampaign(adsObjs, choseCampaign) {
  var fillteredAdsObjs = []
  adsObjs.forEach(function (adsObj) {
    if (adsObj.campaign.id == choseCampaign.id) {
      fillteredAdsObjs.push(adsObj);
    }
  })

  return fillteredAdsObjs;

}

function assignAppObjectAndBannerObjectToAdsObject(adsObjects, next) {
  async.forEach(adsObjects, function(adsObject, callback) {
    redis.findOne('banner' + ':' + adsObject.banner, function (err, bannerObject) {

      if (bannerObject != null) {
        adsObject.banner = bannerObject;
        redis.findOne('app' + ':' + adsObject.banner.app, function (err, appObject) {
          adsObject.app = appObject;
          callback();
        })
      }
    });
  }, function (err) {
    done(err, adsObjects, next);
  });
}

function assignTrafficToAdsObject(adsObjects, next) {
  async.forEach(adsObjects, function(adsObject, callback) {
    redis.findOne('app' + ':' + adsObject.app.id + ':' + 'campaign' + ':' + adsObject.adsgroup.campaign,
      function (err, appAssignToCampaignObject) {

        if (appAssignToCampaignObject != null) {
          adsObject.traffic = appAssignToCampaignObject.traffic;
        } else {
          adsObject.traffic = 0;
        }

        callback();
      });
  }, function (err) {
    done(err, adsObjects, next);
  });
}

function selectAdsBasedOnTraffic(adsObjects, next) {
  var sharing = [];
  for (var i = 0; i < adsObjects.length; i++) {
    var traffic = parseInt(adsObjects[i].traffic);
    for (var j = 0; j < traffic; j++) {
      sharing.push(i);
    }
  }
  var index = getRandomArbitrary(0, sharing.length);
  var choseApp = adsObjects[sharing[index]];
  if (choseApp == undefined) {
    return next(message.NOT_FOUND_BANNER);
  } else {
    return next(null, choseApp);
  }
}

function done(err, result, next) {
  if (err) {
    next(err.message);
  } else if (!hasResult(result)) {
    next(message.NOT_FOUND_ADS);
  } else {
    next(null, result);
  }
}
function customiseData(adsInfo, deviceInfo, next) {
  var merge = require('merge'),
    appStore = getAppStoreForDevice(adsInfo.app.appstore, deviceInfo.platform);

  var adsInfo = {
    action: adsAction.IMP,
    campaign: adsInfo.campaign.id,
    adsgroup: adsInfo.adsgroup.id,
    ads: adsInfo.id,
    banner: adsInfo.banner.id,
    channel: adsInfo.channel,
    zone: adsInfo.zone,
    app: adsInfo.app.id,
    bannerlink: JSON.parse(adsInfo.banner.file).fd,
    position: deviceInfo.position, // TODO: only for test, please removes this line after test
    re: appStore
  };

  /* for wap, we must add the parameter 'referrer' to googleplay's url to
   * track where installing come from.
   */
  if (deviceInfo.fpid && deviceInfo.platform == 'android'){
    adsInfo.re += "&referrer=" + encodeURIComponent("utm_source=fpid=" + deviceInfo.fpid);
  }

  next(null, merge(adsInfo, deviceInfo));
}

function getAppStoreForDevice(appStrores, platform) {
  var appStrore = JSON.parse(appStrores),
      platform = platform;

  return appStrore[platform];
}

function addParamAppStore(url) {
  var slashCharacter = '/';
  if (url[url.length -1] !== slashCharacter) {
    url += slashCharacter;
  }

  return url;
}

function sendDataToKafka(data) {
  kafka.send(deleteUnnessessaryAttributes(data, ['bannerlink', 're']), kafka.adsTopic);
}

function sendDataToClient(data, res) {
  var qs = require("querystring"),
    bannerLink = data['bannerlink'],
    zone_id = data['zone'],
    apiServer = sails.config.connections.api,
    hostnameAPIServer = 'http://' + apiServer.host +':' + apiServer.port,
    click_url = hostnameAPIServer + '/click?',
    url_view = hostnameAPIServer + '/view?',
    params;

  params = deleteUnnessessaryAttributes(data, ['action', 'ip', 'bannerlink']);
  // TODO: remove this line blow for production
  // This codes is used for test only.
  var v = data['position'],
    view;
  if (v == 1) { //popup
    view =  'zone_position_popup';
  } else if (v == 2) {
    view =  'zone_position_top';
  } else if (v == 3) {
    view = 'zone_position_bottom';
  } else {
    view = 'zone_position_landing';
  }


  return res.view(view,
    {
      click_url: encodeURIComponent(click_url + qs.stringify(params)),
      banner_url: bannerLink,
      zone_id: zone_id,
    });

}

function deleteUnnessessaryAttributes(data, listOfUnnessessaryAttributes) {
  var deepcopy = require('deepcopy');
  var tmpData = deepcopy(data);

  listOfUnnessessaryAttributes.forEach(function(attribute) {
    delete tmpData[attribute];
  })

  return tmpData;
}

function hasResult(result) {
  return (result.length != 0) ? true : false;
}

function hasParameter(param)  {
  if (param) {
    return true;
  }
  return false;
}

function getListKeys(Keys) {
  var Ids = [];
  for (var i in Keys) {
    var key = Keys[i];
    var splitedKey = key.split(':');
    Ids.push(splitedKey[splitedKey.length - 1])
  }
  return Ids;
}

function isInDateRange(fromDate, toDate) {
  var moment = require('moment');
  var dateNow =  moment(new Date());
  var fromDate = moment(new Date(fromDate));
  var toDate =  moment(new Date(toDate));

  setHourMinuteSecondeMiliSecondeToZero(dateNow);
  setHourMinuteSecondeMiliSecondeToZero(fromDate);
  setHourMinuteSecondeMiliSecondeToZero(toDate);

  if((dateNow.unix() >= fromDate.unix()) && (dateNow.unix() <= toDate.unix())) {
    return true;
  } else {
    return false;
  }
}

function setHourMinuteSecondeMiliSecondeToZero(dt) {
  dt.set({'hour': 0, 'minute': 0, 'second': 0, 'milisecond': 0});
}

function isInRuntime(runtimeObject) {
  var moment = require('moment');
  var dayOfWeek = moment().day();
  var hourOfNow = moment().hour();

  return (runtimeObject[dayOfWeek][hourOfNow] == '1')  ? true : false;
}

function getRandomArbitrary(min, max) {
  //return a number bettween (min, max) that excludes max.
  return Math.floor(Math.random() * (max - min) + min);
}

