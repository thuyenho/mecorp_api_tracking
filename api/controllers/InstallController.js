/**
 * InstallController
 *
 * @description :: Server-side logic for managing installs
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /*
   * IMPORTANT: using 'create' with the method POST, using 'find' with the method GET.
   * For now, we are using method POST.
   *
   * Also see models/Install.js
   */
  create: function (req, res) {
    var params = req.allParams(),
      requestInfo = JSON.parse(params["request_info"]),
      allParams= {};

    var defaultValue = consts.DEFAULT_VALUE;

    if (requestInfo['platform'] == 'wp') {
      var device_info = utils.parseUserAgent(requestInfo['user_agent']);
      requestInfo['model'] = device_info['device']['model'];
      requestInfo['manufacturer'] = device_info['device']['vendor'];
      requestInfo['version'] = device_info['os']['version'];
    }


    allParams['public_ip'] = params["public_ip"];
    allParams['deviceid'] = requestInfo['device_id'];
    allParams['platform'] = requestInfo['platform'];
    allParams['app'] = requestInfo['app'];
    allParams['db_device_id'] = requestInfo['bd_device_id'] || defaultValue;
    allParams['telco'] = requestInfo['telco'] || defaultValue ;
    allParams['referrer'] = requestInfo['referrer'] || defaultValue;
    allParams['device_model'] = requestInfo['model'] || defaultValue;
    allParams['manufacturer'] = requestInfo['manufacturer'] || requestInfo['device_type'] || defaultValue;

    allParams['version_id'] = requestInfo['bd_version_id'] || requestInfo['version_id'] || defaultValue;
    allParams['private_ip'] = requestInfo['bd_private_ip'] || requestInfo['private_ip'] || requestInfo['ip_user'] || defaultValue;
    allParams['dns'] = requestInfo['bd_dns'] || requestInfo['dns'] || defaultValue;
    allParams['screen_size'] = requestInfo['bd_screen_size'] || requestInfo['screen_size'] || defaultValue;
    allParams['language'] = requestInfo['bd_language'] || requestInfo['language'] || defaultValue;
    allParams['version'] = requestInfo['bd_version'] || requestInfo['version'] || defaultValue;
    allParams['device_type'] = requestInfo['bd_device_type'] || requestInfo['device_type'] || defaultValue;
    allParams['longitude'] = requestInfo['longitude'] || requestInfo['longitude'] || defaultValue;
    allParams['latitude'] = requestInfo['latitude'] || requestInfo['latitude'] || defaultValue;

    var identify = requestInfo['identify'] ? JSON.parse(requestInfo['identify']) : null;
    allParams['imei'] = identify ?  (identify['imei'] || defaultValue) : defaultValue;
    allParams['emails'] = identify ?  (identify['account'] || defaultValue) : defaultValue;


    console.log('---install---');
    console.log(allParams);

    Install.create(allParams).exec(function(err, response) {
      if (err) {
        res.badRequest(err);
      } else {
        res.ok('OK');
      }
    })
  },

  find: function (req, res) {
    return res.notFound();
  },

  update: function (req, res) {
    return res.notFound();
  },

  destroy: function (req, res) {
    return res.notFound();
  }

};

