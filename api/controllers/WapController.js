/**
 * WapController
 *
 * @description :: Server-side logic for managing waps
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  click: function(req, res) {
    return res.view('wap');
  }
};

