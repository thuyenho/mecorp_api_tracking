/**
 * RoleController
 *
 * @description :: Server-side logic for managing roles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /*
   * IMPORTANT: using 'create' with the method POST, using 'find' with the method GET.
   * For now, we are using method POST.
   *
   * Also see models/Install.js
   */
  create: function (req, res) {
    var params = req.allParams();

    Role.create(params).exec(function(err, response) {
      if (err) {
        res.badRequest();
      } else {
        res.ok();
      }
    })
  },

  find: function (req, res) {
    return res.notFound();
  },

  update: function (req, res) {
    return res.notFound();
  },

  destroy: function (req, res) {
    return res.notFound();
  }

};

