/**
 * RegisterController
 *
 * @description :: Server-side logic for managing registers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  // Also see models/Register.js
  create: function (req, res) {
    Register.create(req.allParams()).exec(function (err, response) {
      if (err) {
        res.badRequest(err);
      } else {
        res.ok('OK');
      }
    });
  }
};

