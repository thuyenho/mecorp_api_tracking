/**
 * ClickController
 *
 * @description :: Server-side logic for managing clicks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  // Also see models/Click.js
  find: function (req, res) {
    var allParams = req.allParams();

    // because Api server is behide Nginx Server. we must use param 'x-real-ip' in req header
    allParams['public_ip'] = req.headers['x-real-ip'] || "127.0.0.1";

    console.log('--- click ---')
    console.log(allParams);

    Click.create(allParams).exec(function(err, response) {
      if (err) {
        res.badRequest();
      } else {
        res.redirect(decodeURI(allParams['re']));
      }
    })
  },

  create: function (req, res) {
    return res.notFound();
  },

  update: function (req, res) {
    return res.notFound();
  },

  destroy: function (req, res) {
    return res.notFound();
  }

};

