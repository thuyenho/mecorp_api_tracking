/**
 * ViewController
 *
 * @description :: Server-side logic for managing views
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  // Also see models/View.js
  find: function (req, res) {
    var params = req.allParams();

    // because Api server is behide Nginx Server. we must use param 'x-real-ip' in req header
    params['ip'] = req.headers['x-real-ip'];

    params['action'] = adsAction.VIEW;

    View.create(params).exec(function(err, response) {
      if (err) {
        res.badRequest();
      } else {
        res.ok();
      }
    })
  },

  create: function (req, res) {
    return res.notFound();
  },

  update: function (req, res) {
    return res.notFound();
  },

  destroy: function (req, res) {
    return res.notFound();
  }

};

