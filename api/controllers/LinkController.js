/**
 * LinkController
 *
 * @description :: Server-side logic for managing links
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  find: function (req, res) {
    var params = req.allParams(),
      publisher = params["publisher"],
      campaign = params["campaign"],
      platform = params["platform"],
      fpid = params["fpid"],
      userAgent = req.headers['user-agent'];

    // because Api server is behide Nginx Server. we must use param 'x-real-ip' in req header
    params['public_ip'] = req.headers['x-real-ip'];
    params['source_type'] = consts.SOURCE_TYPE_TRACKING_LINK;


    if (!publisher || !campaign || !platform || !fpid) {
      return res.badRequest();
    }

    redis.get("map:username:" + publisher, function (err, usernameId) {
      if (!usernameId) {
        return res.badRequest(message.NOT_FOUND_USERNAME);
      } else {
        redis.get("map:campaign:" + campaign, function (err, campaignId) {
          if (!campaignId) {
            return res.badRequest(message.NOT_FOUND_CAMPAIGN);
          } else {
            findAppStoreLink(campaignId, platform, userAgent, function (err, appStoreLink) {
              if (err) {
                return res.badRequest(err);
              } else {
                Link.create(params).exec(function(err, response) {
                  if (err) return res.send(err);
                  return res.redirect(appStoreLink +
                                      '&referrer=' +
                                      encodeURIComponent('utm_source=fpid=' +
                                        response.fpid +
                                        '&source_type=' +
                                        consts.SOURCE_TYPE_TRACKING_LINK));
                })
              }
            })
          }
        })
      }
    })
  },

  create: function (req, res) {
    return res.notFound();
  },

  update: function (req, res) {
    return res.notFound();
  },

  destroy: function (req, res) {
    return res.notFound();
  }
};

function findAppStoreLink (campaignId, platform, userAgent, cb) {
  var platform = platform.toLowerCase();

  /*
   * TODO: current appstore for platform we are only based on string : 'windowsphone', 'android', 'ios'
   * But some cases platform is different from string above. e.g. 'windows', 'wp', 'ad'
   * So that, we need to build a function to convert other string to correct formated platform.
   * e.g: convert string 'windows' to 'windowsphone'
   */
  if (platform === 'windows' || platform === 'windows phone') {
    platform = 'windowsphone';
  }

  redis.findOne("campaign:" + campaignId, function(err, campaignObj) {
    if (!campaignObj) {
      cb(message.NOT_FOUND_CAMPAIGN);
    } else {

      // TODO: currently, we always redirect to google play
      platform = 'android';

      redis.get("map:appstorelink:app:" + campaignObj.app + ":platform:" + platform, function (err, appStoreLink) {
        if (!appStoreLink) {
          cb(message.NOT_SUPPORT_PLATFORM + " " + platform);
        } else {
          var MobileDetect = require('mobile-detect'),
            md = new MobileDetect(userAgent);
          if (md.os() == consts.ANDROID_OS) {
            appStoreLink = appStoreLink.replace(consts.GOOGLE_PLAY, consts.GOOGLE_PLAY_DEEPLINK);
          }

          cb(null, appStoreLink);
        }
      })
    }
  })
}
