/**
 * WapclickController
 *
 * @description :: Server-side logic for managing wapclicks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  click: function(req, res) {
    var publisher = req.param('publisher'),
        campaign = req.param('campaign');
    if (!publisher || !campaign) {
      return res.badRequest();
    }

    if (utils.isBot(req)) {
      var http = require('http');

      http.get({
        host: 'backend.boss.mobo.vn',
        path: '/public/campaign?campaign_code=' + campaign
      }, function(response) {
        var body = '';
        response.on('data', function(d) {
            body += d;
        });

        response.on('end', function() {
          var responseData = JSON.parse(body),
            appInfo = responseData['data']
          if (responseData['code'] == 0) {
            return res.badRequest();       
          } 

          return res.view('share_on_facebook',{
            title: appInfo['name'],
            icon: appInfo['icon_image'],
            description: appInfo['description'],
            layout: false
          });
        });
      })

    } else { 

      return res.view('wapclick',{
        publisher: publisher,
        campaign: campaign,
        click_url: '/link?',
        locals: false
      });
    }
  }

};

