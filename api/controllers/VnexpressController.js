/**
 * VnexpressController
 *
 * @description :: Server-side logic for managing vnexpresses
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  click: function(req, res) {
    return res.view('vnexpress');
  }
};

