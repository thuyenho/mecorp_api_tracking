/**
 * Init-impController
 *
 * @description :: Server-side logic for managing init-imps
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    find: function (req, res ) {
      var qs = require("querystring"),
        hostnameAPIServer = sails.config.connections.hostname,
        imp_url = hostnameAPIServer + "/imp?",
        allParams = req.allParams(),
        adUnitId = allParams['ad_unit_id'],
        platform = allParams['platform'],
        isApp = allParams["deviceid"] ? true: false,
        isWap = allParams["fpid"] ? true: false;

        allParams['public_ip'] = req.headers['x-real-ip'] || "127.0.0.1";

      if(!isRequestHasAllParamRequired(allParams) || notHasDeviceInfo(allParams)){
        return res.ok(errInfo("bad request"));
      }

      platform = platform.toLowerCase();
      /*
       * TODO: current appstore for platform we are only based on string : 'windowsphone', 'android', 'ios'
       * But some cases platform is different from string above. e.g. 'windows', 'wp', 'ad'
       * So that, we need to build a function to convert other string to correct formated platform.
       * e.g: convert string 'windows' to 'windowsphone'
       */
      if (platform === 'windows' || platform === 'windows phone') {
        platform = 'windowsphone';
      }

      async.waterfall([

        function(next) {
          findCampaignsBelongToChannel(adUnitId, next);
        },

        function(campaigns, next) {
          findAdsMatchCampaignConditions(campaigns, next);
        },

        function(campaigns, next) {
          selectCampaignBasedOnTraffic(campaigns, next);
        },

        function(campaign, next) {
          findAppStore(campaign, platform, next);
        },

        function(campaign, next) {
          findBanner(campaign, next);
        },

      ], function (err, campaign) {

        if (err) {
          return res.ok(errInfo(err));
        }

        var isAndroid = (platform == 'android');

        allParams['re'] = campaign["app_store_link"];
        allParams['position'] = campaign["position"];
        allParams['banner_image'] = campaign["banner_image"];
        allParams['pub_campaign'] = campaign["id"];

        /* for wap, we must add the parameter 'referrer' to googleplay's url to
         * track where installing come from.
         */
        if (isWap && isAndroid){
          allParams['re'] += "&referrer=" + encodeURIComponent('utm_source=fpid=' +
                                                                allParams['fpid'] +
                                                                '&source_type=' + consts.SOURCE_TYPE_ADS_WAP);
        }

        if(isAndroid) {
          // We use google deep link (market://) beacause sometime when user clicks on banner, webview redirects
          // to browser. we expect redirect to Google Play (CH Play)
          // so, we replace https:// by market://
          allParams['re'] =  allParams['re'].replace(consts.GOOGLE_PLAY, consts.GOOGLE_PLAY_DEEPLINK)
        }

        InitImp.create(allParams).exec(function (err, adInfo) {
          if (err) {
            return res.ok(errInfo(err));
          } else {

            return res.send({
              "code": 1,
              "message": 'OK',
              "data": {
                "banner_url": imp_url + qs.stringify(adInfo),
              }
            });
          }
        })

      });
    },

    create: function (req, res) {
      return res.notFound();
    },

    update: function (req, res) {
      return res.notFound();
    },

    destroy: function (req, res) {
      return res.notFound();
    }

};

function findCampaignsBelongToChannel(adUnitId, next) {

  redis.findKeys('ad_unit' + ':' + adUnitId + ':' + 'pubcampaign' + ':' + '*',
    function (err, pubCampaignKeys) {
      if (pubCampaignKeys.length === 0) {
        next(message.NOT_FOUND_ADS);
      } else {
        var campaigns = [];
        async.forEach(pubCampaignKeys, function (campaignchanelKey, callback) {
          redis.findOne(campaignchanelKey, function (err, campaignOjb) {
            if(campaignOjb) {
              campaigns.push(campaignOjb);
            }
            callback();
          });

        }, function (err) {
          done(null, campaigns, next);
        });
      }
    }
  );
}


function findAdsMatchCampaignConditions(campaigns, next) {
  var campaignsMatchConditions = [];
  async.forEach(campaigns, function(campaign, callback) {
    if (isInDateRange(campaign.from_date, campaign.to_date) &&
      isRunningCampaign(campaign.status)  &&
      isInRuntime(campaign.runtime)) {
      campaignsMatchConditions.push(campaign);
    }
    callback();
  }, function (err) {
    done(err, campaignsMatchConditions, next);
  });
}

function isRunningCampaign(status) {
  return parseInt(status) == consts.CAMPAIGN_STATUS_RUNNING ? true : false;
}
function isInDateRange(fromDate, toDate) {
  var moment = require('moment'),
    dateNow =  moment(new Date()),
    fromDate = moment(new Date(fromDate)),
    toDate =  moment(new Date(toDate));

  setHourMinuteSecondeMiliSecondeToZero(dateNow);
  setHourMinuteSecondeMiliSecondeToZero(fromDate);
  setHourMinuteSecondeMiliSecondeToZero(toDate);

  if((dateNow.unix() >= fromDate.unix()) && (dateNow.unix() <= toDate.unix())) {
    return true;
  } else {
    return false;
  }
}

function setHourMinuteSecondeMiliSecondeToZero(dt) {
  dt.set({'hour': 0, 'minute': 0, 'second': 0, 'milisecond': 0});
}

function isInRuntime(runtime) {
  var runtime = JSON.parse(runtime),
    moment = require('moment'),
    dayOfWeek = moment().day(),
    hourOfNow = moment().hour();

  return (runtime[dayOfWeek][hourOfNow] == '1')  ? true : false;
}

function selectCampaignBasedOnTraffic(campaigns, next) {
  var sharing = [];
  for (var i = 0; i < campaigns.length; i++) {
    var traffic = parseInt(campaigns[i].traffic);
    for (var j = 0; j < traffic; j++) {
      sharing.push(i);
    }
  }
  var index = getRandomArbitrary(0, sharing.length);
  var campaign = campaigns[sharing[index]];
  if (campaign == undefined) {
    return next(message.NOT_FOUND_BANNER);
  } else {
    return next(null, campaign);
  }
}

function getRandomArbitrary(min, max) {
  //return a number bettween (min, max) that excludes max.
  return Math.floor(Math.random() * (max - min) + min);
}


function findAppStore(pubCampaign, platform, next) {
  redis.findOne("businesspolicy:" + pubCampaign.business_policy, function (err, businesspolicy) {
    redis.findOne("campaign:" + businesspolicy.campaign , function (err, campaign) {
      redis.get("map:appstorelink:app:" + campaign.app + ":platform:" + platform, function (err, appStoreLink) {
        if (!appStoreLink) {
          next(message.NOT_SUPPORT_PLATFORM + " " + platform);
        } else {
          pubCampaign['app_store_link'] = appStoreLink;
          next(null, pubCampaign);
        }
      })
    });
  });
}

function findBanner(campaign, next) {
  redis.findOne("banner:" + campaign.banner , function (err, banner) {
    if (!banner) {
      next(message.NOT_FOUND_BANNER);
    } else {
      campaign['banner_image'] = banner.image;
      campaign['position'] = banner.position;
      next(null, campaign);
    }
  })
}


function isRequestHasAllParamRequired(params) {
  var listParams = Object.getOwnPropertyNames(params),
    listPramsRequired = ["ad_unit_id", "platform", "public_ip"];

  for (var i=0; i<listPramsRequired.length; i++) {
    if (listParams.indexOf(listPramsRequired[i]) == -1) {
      return false;
    }
  }
  return true;
}

function notHasDeviceInfo(params) {
  return (params["deviceid"] || params["fpid"]) ? false : true;
}

function errInfo(errMsg) {
  return {
    "code": 0,
    "message": errMsg
  };
}

function done(err, result, next) {
  if (err) {
    next(err.message);
  } else if (!hasResult(result)) {
    next(message.NOT_FOUND_ADS);
  } else {
    next(null, result);
  }
}


function hasResult(result) {
  return (result.length != 0) ? true : false;
}
