/**
 * LoginController
 *
 * @description :: Server-side logic for managing logins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  // Also see models/Login.js
  create: function (req, res) {
    Login.create(req.allParams()).exec(function (err, response) {
      if (err) {
        res.badRequest(err);
      } else {
        res.ok('OK');
      }
    });
  }
};

