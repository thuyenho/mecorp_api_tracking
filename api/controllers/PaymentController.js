/**
 * PaymentController
 *
 * @description :: Server-side logic for managing payments
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  // Also see models/Payment.js
  create: function (req, res) {
    console.log('---payment---');
    console.log(req.allParams());
    Payment.create(req.allParams()).exec(function (err, response) {
      if (err) {
        console.log('----payment-error-----');
        console.log(err);
        res.badRequest(err);
      } else {
        res.ok("OK");
      }
    });
  }

};

