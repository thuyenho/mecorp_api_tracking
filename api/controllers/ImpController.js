/**
 * ImpController
 *
 * @description :: Server-side logic for managing imps
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  find: function (req, res) {
    console.log("--- IMPRESION ---");
    console.log(req.allParams());
    var qs = require("querystring"),
    hostnameAPIServer = sails.config.connections.hostname;

    var  click_url = hostnameAPIServer + "/click?",
      allParams = req.allParams(),
      banner_view ;

    // TODO: only for test on localhost. For production, remove this line below
    allParams['public_ip'] = req.headers['x-real-ip'] || "127.0.0.1";

    Imp.create(allParams).exec(function(err, response) {
      if (err) {
        res.badRequest(err);
      } else {
        if(response['position'] == consts.AD_TYPE_BANNER) {
          banner_view = "ad_type_banner";
        } else {
          banner_view = "ad_type_popup";
        }
        return res.view(banner_view,
          {
            click_url: encodeURIComponent(click_url + qs.stringify(allParams)),
            banner_url: response['banner_image'],
            ad_unit_id: response['ad_unit_id'],
            is_wap: allParams["fpid"] ? true: false
          });
      }
    })
  },

  create: function (req, res) {
    return res.notFound();
  },

  update: function (req, res) {
    return res.notFound();
  },

  destroy: function (req, res) {
    return res.notFound();
  }

};
