module.exports = function (req, res, next) {

  var accessToken = req.param('access_token'),
    apiAccessToken = sails.config.connections.accessToken;

  if (accessToken !== apiAccessToken) {
    return res.forbidden('You are not permitted to perform this action.');
  } else {
    return next();
  }
}

