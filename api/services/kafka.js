var kafkaConnection = sails.config.connections.kafka;
var kafka = require('kafka-node'),
  Producer = kafka.Producer,
  client = new kafka.Client(kafkaConnection.host + ':' + kafkaConnection.port),
  producer = new Producer(client);



producer.on('ready', function () {
  console.log('Kafka is ready');
})

// producer.on('error', function (err) {
//   console.log('Error occurs while client is connecting to kafka server:', err);
// });


module.exports = {
  adsTopic: 'ads-topic',
  registerTopic: 'register-topic',
  loginTopic: 'login-topic',
  paymentTopic: 'payment-topic',
  roleTopic: 'role-topic',
  trackingLinkTopic: 'tracking-link-topic',

  send: function (data, topic) {
    var currentDatetime = new Date();
    data['@timestamp']= currentDatetime;

    var payloads = [
      { topic: topic,
        messages: JSON.stringify(data),
        partition: 0
      },
    ];

    producer.send(payloads, function (err, res) {
      console.log('the response from kafka server: ', res);
    });

  }
}
