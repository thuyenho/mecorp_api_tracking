module.exports = {
	DEFAULT_VALUE: 'unknown',
  CAMPAIGN_STATUS_RUNNING: 1,

  AD_TYPE_BANNER: 0,
  AD_TYPE_POPUP: 1,

  CPS_SIZE_SMALL: 1,
  CPS_SIZE_MEDIUM: 2,
  CPS_SIZE_LARGE: 3,

  SOURCE_TYPE_TRACKING_LINK: 1,
  SOURCE_TYPE_ADS_WAP: 2,

  GOOGLE_PLAY: 'https://play.google.com/store/apps/',
  GOOGLE_PLAY_DEEPLINK: 'market://',

  ANDROID_OS: 'AndroidOS'

}
