var redisConnection = sails.config.connections.redis;
var redis = require("redis"),
  client = redis.createClient(redisConnection.port, redisConnection.host),
  redisPrefix = redisConnection.prefix;

client.on("error", function (err) {
  console.log("Redis Error: " + err);
});

client.on("ready", function (err) {
  console.log("Redis is ready.");
});

module.exports = {
  findKeys: function (key, callback) {
    var re = new RegExp("mts:"),
      key = re.test(key) ? key: redisPrefix + key;
    client.keys(key, callback);
  },

  findOne: function (key, callback) {
    var re = new RegExp("mts:"),
      key = re.test(key) ? key: redisPrefix + key;
    client.hgetall(key, callback);
  },

  get: function (key, callback) {
    var re = new RegExp("mts:"),
      key = re.test(key) ? key: redisPrefix + key;
    client.get(key, callback);
  },
}
