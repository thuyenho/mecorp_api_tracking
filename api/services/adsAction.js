/**
 * Created by thuyenhv on 7/23/15.
 */

module.exports = {
  IMP: 'imp',
  CLICK: 'click',
  VIEW: 'view',
  INSTALL: 'install',

  TRACKING_LINK_CLICK: 'tracking-click-click'
}
