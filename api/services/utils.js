var md5 = require('MD5');

module.exports = {
  convertValuesOfObjectIntoLowerCase: function (obj) {
    for (key in obj) {
      var value = obj[key];
      if (typeof  value === 'string') {
        obj[key] = value.toLowerCase();
      }
    }
  },

  convertEmptyValueIntoUnknownString: function (values, attributes) {
    for (attr in attributes) {
      if (!values[attr]) {
        values[attr] = consts.DEFAULT_VALUE
      }
    }
  },

  getInstallData: function(requestData) {
    var requestInfo = JSON.parse(requestData["request_info"]),
      publicIp = requestData["public_ip"];

    return utils.parseParamsOfPlatform(requestInfo, publicIp);
  },

  isBot: function (req) {
    var stringContainInUserAgentFacebook = 'facebookexternalhit',
        stringContainInUserAgentGoogle = 'developers\\.google\\.com\\/\\+\\/web\\/snippet\\/', 
      re = new RegExp(stringContainInUserAgentFacebook + '|' + stringContainInUserAgentGoogle);

    return re.test(req.headers['user-agent'])

  },

  parseParamsOfPlatform: function(requestInfo, publicIp) {
    var public_ip = publicIp,
      deviceid = requestInfo["device_id"],
      platform = requestInfo["platform"],
      app = requestInfo["app"],
      referrer = requestInfo["referrer"] || null;

    return {
      "deviceid": deviceid,
      "platform": platform,
      "app": app,
      "public_ip": public_ip,
      "referrer": referrer

      /*
       "private_ip": private_ip,
       "language": language,
       "longitude": longitude,
       "latitude": latitude,
       "private_ip": private_ip
       */
    };
  },

  parseUserAgent: function (userAgent) {
    var parser = require('ua-parser-js');
    return parser(userAgent);
  }

}
