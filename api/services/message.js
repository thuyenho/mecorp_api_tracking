/**
 * Created by thuyenhv on 7/22/15.
 */

module.exports = {
  NOT_ENOUGH_PARAMS: "Not enough params",
  NOT_FOUND_PARAM_ZONEID: "We didn't find the parameter zoneid.",
  NOT_FOUND_ADS: "We didn't find any ads.",
  NOT_FOUND_BANNER: "We didn't find banner.",
  NOT_FOUND_CAMPAIGN: "Not Found Campaign",
  NOT_SUPPORT_PLATFORM: "This app is not support your operating system.",
  NOT_FOUND_APP: "This app doesn't exist.",
  NOT_FOUND_CAMPAIGN: "This campaign doesn't exist.",
  NOT_FOUND_USERNAME: "This username doesn't exist.",
  NOT_SUPPORT_PLATFORM: "This campaign do not support for platform"

}
